(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/about-toko/about-toko.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/about-toko/about-toko.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header>\n    <!-- Navbar -->\n</header>\n<div class=\"wrapper vh-200\">\n    <div class=\"container-fluid\">\n        <div class=\"headToko\">\n\n            <div class=\"row headProfil\">\n                <div class=\"col-lg-3 d-flex justify-content-center picture\">\n                    <img src=\"assets/pic/mina.jpg\" class=\"rounded-circle ava\" alt=\"ava\" id=\"ava\">\n                </div>\n                <div class=\"col-lg-9 desc\">\n                    <div class=\"title\">\n                        <h1>Loutaro</h1>\n                    </div>\n                    <div class=\"row mid\">\n                        <div class=\"col-md-3 followers\">\n                            <p><b>100</b> Produk</p>\n                        </div>\n                        <div class=\"col-md-3\">\n                            <p><b>10</b> Pengikut</p>\n                        </div>\n                    </div>\n                    <div class=\"desc1\">\n                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Earum aliquid nam facere laborum placeat, accusantium iste incidunt sit veritatis maiores inventore ut nihil numquam quo fugit ullam similique! Magnam, quaerat!</p>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <hr>\n        <div class=\"navbar d-flex justify-content-center\">\n            <div class=\"col-2 produk\"><a routerLink=\"/profileToko\">Produk</a></div>\n            <div class=\"col-2 ulasan\"><a routerLink=\"/profileUlasan\">Ulasan</a></div>\n            <div class=\"col-2 about\"><a routerLink=\"/profileAbout\">About</a></div>\n        </div>\n        <h4>Info  Toko</h4>\n        <hr>\n        <div class=\"row des\">\n            <div class=\"col-md subTitle\">\n                <h4>Deskripsi toko</h4>\n                Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis dolore perferendis totam error autem repellat necessitatibus, nam dolor debitis exercitationem. Sunt iusto consequatur illum. Dolores doloremque distinctio labore optio eius.\n            </div>\n            <div class=\"col-md subTitle\">\n                <h4>Alamat Toko</h4>\n                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Velit eaque magni rem ducimus animi, unde consectetur numquam. Voluptatum repellat voluptates molestias nisi, doloremque hic rerum perspiciatis iste praesentium quod. Quasi.</p>\n                <h5 class=\"subTitle\">Buka sejak <small class=\"text-muted\">1945</small></h5>\n                <p></p>\n            </div>\n        </div>\n    </div>\n    \n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<router-outlet></router-outlet>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<div class=\"container-fluid wrapper\">\n  <div class=\"row\">\n    <div class=\"col-md vh-100 bagian1\">\n\n      <div class=\"wrapper1\">\n        <div class=\"logo\">\n          <img src=\"assets/pic/logo-test.png\" alt=\"logoLoutaro\" id=\"logoLoutaro\">\n        </div>\n        <div class=\"textSlogan\">\n          <h3>Jual-Beli desain digital <br> Pekerja Digital</h3>\n        </div>\n      </div>\n\n\n    </div>\n    <div class=\"col-md-7 vh-100 bagian2\">\n      <div class=\"row justify-content-md-center atas\">\n\n        <div class=\"col-lg\"></div>\n\n        <div class=\"col-lg-6 \">\n\n          <div class=\"bagianatas\">\n            <div class=\"logob\">\n              <img src=\"assets/pic/logo-test.png\" alt=\"logo\" id=\"logoLoutaro\">\n            </div>\n            <h4>Login in to Loutaro</h4>\n\n            <div class=\"mediasosial\">\n              <div class=\"google \">\n                <img src=\"assets/pic/google-plus.png\" alt=\"google\">\n                <p>Masuk dengan google</p>\n              </div>\n              <div class=\"facebook\">\n                <img src=\"assets/pic/facebook.png\" alt=\"facebook\">\n              </div>\n            </div>\n          </div>\n\n          <div class=\"pemisah\">\n            <div class=\"row\">\n              <div class=\"col-5\"><hr></div>\n              <div class=\"col-2\"><p>Atau</p></div>\n              <div class=\"col-5\"><hr></div>\n            </div>\n          </div>\n\n\n          <form>\n            <div class=\"form-group\">\n              <label for=\"exampleInputEmail1\">Username atau Email address</label>\n              <input type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\">\n            </div>\n            <div class=\"form-group\">\n              <label for=\"exampleInputPassword1\">Password</label>\n              <input type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\">\n            </div>\n            <button type=\"Login\" class=\"btn btn-primary btn-block\">Login</button>\n          </form>\n\n\n        <div class=\"bagianBawah\">\n          <p>Tidak punya akun? <a routerLink =\"/register\">Buat akun</a></p>\n        </div>\n\n      </div>\n      <div class=\"col-lg\"></div>\n      </div>\n    </div>\n  </div>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/metode-pembayaran/metode-pembayaran.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/metode-pembayaran/metode-pembayaran.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--Navbar-->\n\n<div class=\"container\">\n    <div class=\"row wrapper\">\n\n        <div class=\"col-lg-7 bagian1\">\n            <div class=\"col detailPembayaran\">\n                <h5>Detail pembayaran</h5>\n                <div class=\"borderCatatan\">\n                    <div class=\"iconCart\">\n                        <img src=\"assets/icon/shopping-cart.png\" alt=\"cartIcon\" id=\"cartIcon\">\n                    </div>\n                    <div class=\"catatan\">\n                        <p>Jika mau perbaruin pembayaranmu, silakan lakukan submit ulang <a routerLink=\"#\">Pembayaran</a></p>\n                    </div>\n                </div>\n\n                <div class=\"identitas\">\n                    <p id=\"alamat\">Myoui Arya <br> Jl. SudahdiAspal SampingRumah No 1/2 <br> Medan, Sumatera Utra 240397 <br> Indonesia </p>\n                    <p id=\"kodePembayaran\"> Kode Pembayaran <strong>#MYOUIARYA24</strong> </p>\n                </div>\n\n            </div>\n            <div class=\"col metodePembayaran\">\n                <div class=\"row metodeP\">\n                    <div class=\"col col-8 metodePText\">\n                        <h5>Metode Pembayaran</h5>\n                    </div>\n                    <div class=\"col col-4\">\n                        <select class=\"form-control\">\n                            <option>E-Money</option>\n                            <option>Etc</option>\n                            <option>Etc</option>\n                          </select>\n                    </div>\n                </div>\n                <hr>\n                <div class=\"row justify-content-lg-center Emoney\">\n                    <div class=\"col dana text-center\">\n                        <img src=\"assets/E-money/dana.jpeg\" alt=\"dana\">\n                    </div>\n                    <div class=\"col gopay text-center\">\n                        <img src=\"assets/E-money/Gopay.jpeg\" alt=\"gopay\">\n                    </div>\n                    <div class=\"col ovo text-center\">\n                        <img src=\"assets/E-money/ovo.png\" alt=\"ovo\">\n                    </div>\n                    <div class=\"col linkaja text-center\">\n                        <img src=\"assets/E-money/linkaja.png\" alt=\"\">\n                    </div>\n                </div>\n                \n            </div>\n        </div>\n\n        <div class=\"col-lg-5 bagian2\">\n            <div class=\"col pembayaran table-responsive\">\n                <h5>Pesanan</h5>\n                <hr id=\"garis\">\n\n                <div class=\"table-responsive  listHarga\">\n                    <table class=\"table \">\n                        <tbody>\n                            <tr>\n                                <th scope=\"row\">1</th>\n                                <td>Shadowfy - Blur & Shadow</td>\n                                <td>Rp. 185.000</td>\n                            </tr>\n    \n                            <tr>\n                                <th scope=\"row\">2</th>\n                                <td>Procreate Chareacter Draw</td>\n                                <td>Rp. 240.000</td>\n                            </tr>\n                        </tbody>\n                    </table>\n                    \n                </div>\n                <hr id=\"garis\">\n                \n                <div class=\"input-group mb-3\">\n                    <input type=\"text\" class=\"form-control\">\n                    <div class=\"input-group-append\">\n                      <button class=\"btn btn-success\" type=\"button\">VOUCHER</button>\n                    </div>\n                  </div>\n\n                <div class=\"table-responsive totalHarga\">\n                    <table class=\"table\">\n                        <tbody>\n                            <tr>\n                                <th scope=\"row\"></th>\n                                <td><h4>Total Harga</h4></td>\n                                <td><h4>Rp. 535.000</h4></td>\n                            </tr>\n                        </tbody>\n                    </table>\n                </div>\n\n                <button type=\"button\" class=\"btn btn-success btn-block\" id=\"beli\">Beli</button>\n\n                \n\n                \n            </div>\n            \n        </div>\n\n\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/produk-ptoko/produk-ptoko.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/produk-ptoko/produk-ptoko.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header>\n    <!-- Navbar -->\n</header>\n<div class=\"wrapper v-200\">\n    <div class=\"container\">\n        <div class=\"col kategori\">\n            <h3>Banner</h3>\n            <hr>\n        </div>\n        <div class=\"row listItem\">\n            <div class=\"row\">\n                <div class=\"col-md-4\">\n                  <div class=\"thumbnail\">\n                    <a routeLink>\n                      <img src=\"assets/pic/dummy.jpg\" alt=\"banner\" style=\"width:100%\">\n                      <div class=\"caption\">\n                        <h5>Rp. 60.000</h5>\n                        <small>by loutaro</small> \n                        <p>Lorem ipsum...</p>\n                      </div>\n                    </a>\n                  </div>\n                </div>\n            <div class=\"col-md-4\">\n                <div class=\"thumbnail\">\n                  <a routeLink>\n                    <img src=\"assets/pic/dummy.jpg\" alt=\"banner\" style=\"width:100%\">\n                    <div class=\"caption\">\n                      <h5>Rp. 60.000</h5>\n                      <small>by loutaro</small>\n                      <p>Lorem ipsum...</p>\n                    </div>\n                  </a>\n                </div>\n              </div>\n              <div class=\"col-md-4\">\n                <div class=\"thumbnail\">\n                  <a routeLink>\n                    <img src=\"assets/pic/dummy.jpg\" alt=\"banner\" style=\"width:100%\">\n                    <div class=\"caption\">\n                      <h5>Rp. 60.000</h5>\n                      <small>by loutaro</small>\n                      <p>Lorem ipsum...</p>\n                    </div>\n                  </a>\n                </div>\n              </div>\n        </div>\n\n        <div class=\"col kategori\">\n            <h3>Banner</h3>\n            <hr>\n        </div>\n        <div class=\"row listItem\">\n            <div class=\"row\">\n                <div class=\"col-md-4\">\n                  <div class=\"thumbnail\">\n                    <a routeLink>\n                      <img src=\"assets/pic/dummy.jpg\" alt=\"banner\" style=\"width:100%\">\n                      <div class=\"caption\">\n                        <h5>Rp. 60.000</h5>\n                        <small>by loutaro</small>\n                        <p>Lorem ipsum...</p>\n                      </div>\n                    </a>\n                  </div>\n                </div>\n            <div class=\"col-md-4\">\n                <div class=\"thumbnail\">\n                  <a routeLink>\n                    <img src=\"assets/pic/dummy.jpg\" alt=\"banner\" style=\"width:100%\">\n                    <div class=\"caption\">\n                      <h5>Rp. 60.000</h5>\n                      <small>by loutaro</small>\n                      <p>Lorem ipsum...</p>\n                    </div>\n                  </a>\n                </div>\n              </div>\n              <div class=\"col-md-4\">\n                <div class=\"thumbnail\">\n                  <a routeLink>\n                    <img src=\"assets/pic/dummy.jpg\" alt=\"banner\" style=\"width:100%\">\n                    <div class=\"caption\">\n                      <h5>Rp. 60.000</h5>\n                      <small>by loutaro</small>\n                      <p>Lorem ipsum...</p>\n                    </div>\n                  </a>\n                </div>\n              </div>\n        </div>\n\n    </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile-toko/profile-toko.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile-toko/profile-toko.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header>\n    <!-- Navbar -->\n</header>\n<div class=\"wrapper vh-200\">\n    <div class=\"container-fluid\">\n        <div class=\"headToko\">\n\n            <div class=\"row headProfil\">\n                <div class=\"col-lg-3 d-flex justify-content-center picture\">\n                    <img src=\"assets/pic/mina.jpg\" class=\"rounded-circle ava\" alt=\"ava\" id=\"ava\">\n                </div>\n                <div class=\"col-lg-9 desc\">\n                    <div class=\"title\">\n                        <h1>Loutaro</h1>\n                    </div>\n                    <div class=\"row mid\">\n                        <div class=\"col-md-3 followers\">\n                            <p><b>100</b> Produk</p>\n                        </div>\n                        <div class=\"col-md-3\">\n                            <p><b>10</b> Pengikut</p>\n                        </div>\n                    </div>\n                    <div class=\"desc1\">\n                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Earum aliquid nam facere laborum placeat, accusantium iste incidunt sit veritatis maiores inventore ut nihil numquam quo fugit ullam similique! Magnam, quaerat!</p>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <hr class=\"garis\">\n        <div class=\"navbar d-flex justify-content-center\">\n            <div class=\"col-2 produk\"><a routerLink=\"#\">Produk</a></div>\n            <div class=\"col-2 ulasan\"><a routerLink=\"/profileUlasan\">Ulasan</a></div>\n            <div class=\"col-2 about\"><a routerLink=\"/profileAbout\">About</a></div>\n        </div>\n    </div>\n    <app-produk-ptoko></app-produk-ptoko>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container-fluid wrapper\">\n    <div class=\"row\">\n    <div class=\"col-md vh-100 bagian1\">\n\n        <div class=\"wrapper1\">\n        <div class=\"logo\">\n            <img src=\"assets/pic/logo-test.png\" alt=\"logoLoutaro\" id=\"logoLoutaro\">\n        </div>\n        <div class=\"textSlogan\">\n            <h3>Jual-Beli desain digital <br> Pekerja Digital</h3>\n        </div>\n        </div>\n\n\n    </div>\n    <div class=\"col-md-7 vh-100 bagian2\">\n        <div class=\"row justify-content-md-center atas\">\n        <div class=\"col-lg\"></div>\n\n        <div class=\"col-lg-6 \">\n\n            <div class=\"bagianatas\">\n            <div class=\"logob\">\n                <img src=\"assets/pic/logo-test.png\" alt=\"logo\" id=\"logoLoutaro\">\n            </div>\n            <h4>Login in to Loutaro</h4>\n\n            <div class=\"mediasosial\">\n                <div class=\"google\">\n                <img src=\"assets/pic/google-plus.png\" alt=\"google\">\n                <p>Masuk dengan google</p>\n                </div>\n                <div class=\"facebook\">\n                <img src=\"assets/pic/facebook.png\" alt=\"facebook\">\n                </div>\n            </div>\n            </div>\n\n            <div class=\"pemisah\">\n            <div class=\"row\">\n                <div class=\"col-5\"><hr></div>\n                <div class=\"col-2\"><p>Atau</p></div>\n                <div class=\"col-5\"><hr></div>\n            </div>\n            </div>\n\n\n            <form>\n            <div class=\"row form-group\">\n                <div class=\"col\">\n                    <label for=\"exampleInputName\">Name</label>\n                    <input type=\"text\" class=\"form-control\">\n                </div>\n                <div class=\"col\">\n                    <label for=\"exampleInputUsername\">Username</label>\n                    <input type=\"text\" class=\"form-control\">\n                </div>\n            </div>\n\n            <label for=\"exampleInputEmail1\">Email address</label>\n            <input type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\">\n\n            <div class=\"form-group\">\n                <label for=\"exampleInputPassword1\">Password</label>\n                <input type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\">\n            </div>\n\n            <div class=\"form-check\">\n                <input type=\"checkbox\" class=\"form-check-input\" id=\"exampleCheck1\">\n                <label class=\"form-check-label\" for=\"exampleCheck1\"></label>\n                <small>Creating an account means you’re okay with our Terms of <br> Service, Privacy Policy, and our default Notification Settings.</small> \n            </div>\n\n            <button type=\"Login\" class=\"btn btn-primary btn-block\" id=\"btnAkunB\">Buat Akun Baru</button>\n\n\n            </form>\n\n        </div>\n        <div class=\"col-lg\"></div>\n        </div>\n    </div>\n    </div>\n</div>\n\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/support/support.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/support/support.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header>\n    <!-- Navbar -->\n</header>\n<div class=\"wrapper vh-200\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-2\"></div>\n\n            <div class=\"col-md-7 wrapper1\">\n                <div class=\"head\">\n                    <h4>Need some support ?</h4>\n                </div>\n                <hr>\n                <div class=\"content\">\n                    <div class=\"kodePembelian space\">\n                        <h6 id=\"subTitle\">Masukkan Kode Pembelian</h6>\n                        <p>Masukkan Kode Pembelian disini untuk menverifikasi pembelian kamu</p>\n                        <input type=\"text\" class=\"form-control\" placeholder=\"Masukkan Kode Pembelian\">  \n                    </div>\n                    <div class=\"registrasi space\">\n                        <h6 id=\"subTitle\">Registrasi</h6>\n                        <p>Masukkan username akun kamu untuk mendapatkan bantuan</p>\n                        <form>\n                            <div class=\"row r1\">\n                                <div class=\"col-md-6\">\n                                    <input type=\"text\" class=\"form-control\" id=\"nama\" placeholder=\"Nama kamu ...\">\n                                </div>\n                                <div class=\"col-md-6\">\n                                    <input type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Alamat Email ...\">\n                                </div>\n                            </div>\n                            <div class=\"row r2\">\n                                <div class=\"col-md-6\">\n                                    <input type=\"password\" class=\"form-control\" id=\"password\" placeholder=\"Password ...\">\n                                </div>\n                                <div class=\"col-md-6\">\n                                    <input type=\"password\" class=\"form-control\" id=\"Rpassword\" placeholder=\"Ulangin Password ...\">\n                                </div>\n                            </div>\n                            <div class=\"kategori space\">\n                                <h6 id=\"subTitle\">Kategori Pembelian</h6>\n                                <p>Secara general, kategori pembelian tentang ?</p>\n                                <input type=\"text\" class=\"form-control\" id=\"kategori\" placeholder=\"Kategori Pembelian\">\n                            </div>\n                            <div class=\"penjelasan space\">\n                                <h6 id=\"subTitle\">Pembelian Deskripsi</h6>\n                                <p>Deskripsikan sejelas mungkin mengenai pemeblian ini</p>\n                                <textarea class=\"form-control\" id=\"detailP\" rows=\"3\"></textarea>\n\n                            </div>\n                            <div class=\"submit\">\n                                <button type=\"button\" class=\"btn btn-success btn-block space\" id=\"submit\">Submit</button>\n                            </div>\n                            \n                        </form>\n\n                    </div>\n\n                </div>\n\n            </div>\n\n            <div class=\"col-2\"></div>\n        </div>\n    </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/ulasan-toko/ulasan-toko.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/ulasan-toko/ulasan-toko.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header>\n    <!-- Navbar -->\n</header>\n<div class=\"wrapper vh-200\">\n    <div class=\"container-fluid\">\n        <div class=\"headToko\">\n\n            <div class=\"row headProfil\">\n                <div class=\"col-lg-3 d-flex justify-content-center picture\">\n                    <img src=\"assets/pic/mina.jpg\" class=\"rounded-circle ava\" alt=\"ava\" id=\"ava\">\n                </div>\n                <div class=\"col-lg-9 desc\">\n                    <div class=\"title\">\n                        <h1>Loutaro</h1>\n                    </div>\n                    <div class=\"row mid\">\n                        <div class=\"col-md-3 followers\">\n                            <p><b>100</b> Produk</p>\n                        </div>\n                        <div class=\"col-md-3\">\n                            <p><b>10</b> Pengikut</p>\n                        </div>\n                    </div>\n                    <div class=\"desc1\">\n                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Earum aliquid nam facere laborum placeat, accusantium iste incidunt sit veritatis maiores inventore ut nihil numquam quo fugit ullam similique! Magnam, quaerat!</p>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <hr class=\"garis\">\n        <div class=\"navbar d-flex justify-content-center\">\n            <div class=\"col-2 produk\"><a routerLink=\"/profileToko\">Produk</a></div>\n            <div class=\"col-2 ulasan\"><a routerLink=\"#\">Ulasan</a></div>\n            <div class=\"col-2 about\"><a routerLink=\"/profileAbout\">About</a></div>\n        </div>\n        <div class=\"col ulasanD\">\n            <div id=\"ketegori\">\n                <h4>Banner</h4>\n                <hr>\n            </div>\n            <div class=\"media\">\n                <img src=\"assets/pic/dummy.jpg\" class=\"mr-3\" alt=\"ulasanD\" id=\"imgUlasan\">\n                <div class=\"media-body\">\n                  <h5 class=\"mt-0\">judul Banner</h5>\n                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.\n                </div>\n            </div>\n            <div class=\"media\">\n                <img src=\"assets/pic/dummy.jpg\" class=\"mr-3\" alt=\"ulasanD\" id=\"imgUlasan\">\n                <div class=\"media-body\">\n                  <h5 class=\"mt-0\">judul Banner</h5>\n                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.\n                </div>\n            </div>\n            <div class=\"media\">\n                <img src=\"assets/pic/dummy.jpg\" class=\"mr-3\" alt=\"ulasanD\" id=\"imgUlasan\">\n                <div class=\"media-body\">\n                  <h5 class=\"mt-0\">judul Banner</h5>\n                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.\n                </div>\n            </div>\n\n            \n        </div>\n    </div>\n    \n</div>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./node_modules/webpack/hot sync ^\\.\\/log$":
/*!*************************************************!*\
  !*** (webpack)/hot sync nonrecursive ^\.\/log$ ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./log": "./node_modules/webpack/hot/log.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/webpack/hot sync ^\\.\\/log$";

/***/ }),

/***/ "./src/app/about-toko/about-toko.component.scss":
/*!******************************************************!*\
  !*** ./src/app/about-toko/about-toko.component.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wrapper {\n  margin-top: 2em;\n}\n.wrapper p {\n  font-size: 20px;\n  margin: 0px;\n}\n.wrapper h1, .wrapper h2, .wrapper h3, .wrapper h4, .wrapper h5, .wrapper h6 {\n  margin: 0px;\n}\n.wrapper .container-fluid {\n  width: 70%;\n}\n.wrapper .container-fluid .headToko .headProfil .picture {\n  padding: 10px;\n  width: 140px;\n}\n.wrapper .container-fluid .headToko .headProfil .picture .ava {\n  width: 150px;\n  border: 2px solid gray;\n}\n.wrapper .container-fluid .headToko .headProfil .desc .desc1 {\n  margin-top: 10px;\n}\n.wrapper .container-fluid .garis {\n  margin: 2em 0em;\n}\n.wrapper .container-fluid .navbar {\n  width: 100%;\n}\n.wrapper .container-fluid .navbar .about a {\n  border-top: 1px solid gray;\n}\n.wrapper .container-fluid .navbar a {\n  cursor: pointer;\n  padding-top: 10px;\n  color: black;\n  text-decoration: none;\n}\n.wrapper .container-fluid .navbar a:hover {\n  border-top: 1px solid gray;\n}\n.wrapper .container-fluid .des .subTitle {\n  margin-top: 2em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tZWRpYS9mYXJoYW5hcnlhL0RhdGEvcHJveWVrL3BrdGkxL0xvZ2luUjEvc3JjL2FwcC9hYm91dC10b2tvL2Fib3V0LXRva28uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2Fib3V0LXRva28vYWJvdXQtdG9rby5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQVFJLGVBQUE7QUNOSjtBRERJO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUNHUjtBRERJO0VBQ0ksV0FBQTtBQ0dSO0FEQ0k7RUFDSSxVQUFBO0FDQ1I7QURFZ0I7RUFDSSxhQUFBO0VBQ0EsWUFBQTtBQ0FwQjtBRENvQjtFQUNJLFlBQUE7RUFDQSxzQkFBQTtBQ0N4QjtBREdvQjtFQUNJLGdCQUFBO0FDRHhCO0FETVE7RUFDSSxlQUFBO0FDSlo7QURNUTtFQUNJLFdBQUE7QUNKWjtBRE1nQjtFQUNJLDBCQUFBO0FDSnBCO0FET1k7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7QUNMaEI7QURRWTtFQUNJLDBCQUFBO0FDTmhCO0FEV1k7RUFDSSxlQUFBO0FDVGhCIiwiZmlsZSI6InNyYy9hcHAvYWJvdXQtdG9rby9hYm91dC10b2tvLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndyYXBwZXJ7XG4gICAgcCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgfVxuICAgIGgxLGgyLGgzLGg0LGg1LGg2e1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICB9XG4gICAgbWFyZ2luLXRvcDogMmVtO1xuXG4gICAgLmNvbnRhaW5lci1mbHVpZHtcbiAgICAgICAgd2lkdGg6NzAlOyAgICAgICAgXG4gICAgICAgIC5oZWFkVG9rb3tcbiAgICAgICAgICAgIC5oZWFkUHJvZmlse1xuICAgICAgICAgICAgICAgIC5waWN0dXJle1xuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTQwcHg7XG4gICAgICAgICAgICAgICAgICAgIC5hdmF7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTUwcHg7XG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXI6IDJweCBzb2xpZCBncmF5O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5kZXNje1xuICAgICAgICAgICAgICAgICAgICAuZGVzYzF7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5nYXJpc3tcbiAgICAgICAgICAgIG1hcmdpbjoyZW0gMGVtIDtcbiAgICAgICAgfVxuICAgICAgICAubmF2YmFye1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAuYWJvdXR7XG4gICAgICAgICAgICAgICAgYXtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGdyYXk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYXtcbiAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGE6aG92ZXJ7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGdyYXk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAuZGVze1xuICAgICAgICAgICAgLnN1YlRpdGxle1xuICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDJlbTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIFxuICAgIH1cbn0iLCIud3JhcHBlciB7XG4gIG1hcmdpbi10b3A6IDJlbTtcbn1cbi53cmFwcGVyIHAge1xuICBmb250LXNpemU6IDIwcHg7XG4gIG1hcmdpbjogMHB4O1xufVxuLndyYXBwZXIgaDEsIC53cmFwcGVyIGgyLCAud3JhcHBlciBoMywgLndyYXBwZXIgaDQsIC53cmFwcGVyIGg1LCAud3JhcHBlciBoNiB7XG4gIG1hcmdpbjogMHB4O1xufVxuLndyYXBwZXIgLmNvbnRhaW5lci1mbHVpZCB7XG4gIHdpZHRoOiA3MCU7XG59XG4ud3JhcHBlciAuY29udGFpbmVyLWZsdWlkIC5oZWFkVG9rbyAuaGVhZFByb2ZpbCAucGljdHVyZSB7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHdpZHRoOiAxNDBweDtcbn1cbi53cmFwcGVyIC5jb250YWluZXItZmx1aWQgLmhlYWRUb2tvIC5oZWFkUHJvZmlsIC5waWN0dXJlIC5hdmEge1xuICB3aWR0aDogMTUwcHg7XG4gIGJvcmRlcjogMnB4IHNvbGlkIGdyYXk7XG59XG4ud3JhcHBlciAuY29udGFpbmVyLWZsdWlkIC5oZWFkVG9rbyAuaGVhZFByb2ZpbCAuZGVzYyAuZGVzYzEge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLndyYXBwZXIgLmNvbnRhaW5lci1mbHVpZCAuZ2FyaXMge1xuICBtYXJnaW46IDJlbSAwZW07XG59XG4ud3JhcHBlciAuY29udGFpbmVyLWZsdWlkIC5uYXZiYXIge1xuICB3aWR0aDogMTAwJTtcbn1cbi53cmFwcGVyIC5jb250YWluZXItZmx1aWQgLm5hdmJhciAuYWJvdXQgYSB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCBncmF5O1xufVxuLndyYXBwZXIgLmNvbnRhaW5lci1mbHVpZCAubmF2YmFyIGEge1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBjb2xvcjogYmxhY2s7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cbi53cmFwcGVyIC5jb250YWluZXItZmx1aWQgLm5hdmJhciBhOmhvdmVyIHtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGdyYXk7XG59XG4ud3JhcHBlciAuY29udGFpbmVyLWZsdWlkIC5kZXMgLnN1YlRpdGxlIHtcbiAgbWFyZ2luLXRvcDogMmVtO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/about-toko/about-toko.component.ts":
/*!****************************************************!*\
  !*** ./src/app/about-toko/about-toko.component.ts ***!
  \****************************************************/
/*! exports provided: AboutTokoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutTokoComponent", function() { return AboutTokoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AboutTokoComponent = class AboutTokoComponent {
    constructor() { }
    ngOnInit() {
    }
};
AboutTokoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-about-toko',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./about-toko.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/about-toko/about-toko.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./about-toko.component.scss */ "./src/app/about-toko/about-toko.component.scss")).default]
    })
], AboutTokoComponent);



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _app_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app/login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _app_register_register_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../app/register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _app_metode_pembayaran_metode_pembayaran_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../app/metode-pembayaran/metode-pembayaran.component */ "./src/app/metode-pembayaran/metode-pembayaran.component.ts");
/* harmony import */ var _app_support_support_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../app/support/support.component */ "./src/app/support/support.component.ts");
/* harmony import */ var _app_profile_toko_profile_toko_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../app/profile-toko/profile-toko.component */ "./src/app/profile-toko/profile-toko.component.ts");
/* harmony import */ var _app_produk_ptoko_produk_ptoko_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../app/produk-ptoko/produk-ptoko.component */ "./src/app/produk-ptoko/produk-ptoko.component.ts");
/* harmony import */ var _app_ulasan_toko_ulasan_toko_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../app/ulasan-toko/ulasan-toko.component */ "./src/app/ulasan-toko/ulasan-toko.component.ts");
/* harmony import */ var _app_about_toko_about_toko_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../app/about-toko/about-toko.component */ "./src/app/about-toko/about-toko.component.ts");











const routes = [
    { path: '', component: _app_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'register', component: _app_register_register_component__WEBPACK_IMPORTED_MODULE_4__["RegisterComponent"] },
    { path: 'metodePembayaran', component: _app_metode_pembayaran_metode_pembayaran_component__WEBPACK_IMPORTED_MODULE_5__["MetodePembayaranComponent"] },
    { path: 'support', component: _app_support_support_component__WEBPACK_IMPORTED_MODULE_6__["SupportComponent"] },
    { path: 'profileToko', component: _app_profile_toko_profile_toko_component__WEBPACK_IMPORTED_MODULE_7__["ProfileTokoComponent"] },
    { path: 'profileProduk', component: _app_produk_ptoko_produk_ptoko_component__WEBPACK_IMPORTED_MODULE_8__["ProdukPTokoComponent"] },
    { path: 'profileUlasan', component: _app_ulasan_toko_ulasan_toko_component__WEBPACK_IMPORTED_MODULE_9__["UlasanTokoComponent"] },
    { path: 'profileAbout', component: _app_about_toko_about_toko_component__WEBPACK_IMPORTED_MODULE_10__["AboutTokoComponent"] }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'LoginR1';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _metode_pembayaran_metode_pembayaran_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./metode-pembayaran/metode-pembayaran.component */ "./src/app/metode-pembayaran/metode-pembayaran.component.ts");
/* harmony import */ var _support_support_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./support/support.component */ "./src/app/support/support.component.ts");
/* harmony import */ var _profile_toko_profile_toko_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./profile-toko/profile-toko.component */ "./src/app/profile-toko/profile-toko.component.ts");
/* harmony import */ var _produk_ptoko_produk_ptoko_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./produk-ptoko/produk-ptoko.component */ "./src/app/produk-ptoko/produk-ptoko.component.ts");
/* harmony import */ var _ulasan_toko_ulasan_toko_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./ulasan-toko/ulasan-toko.component */ "./src/app/ulasan-toko/ulasan-toko.component.ts");
/* harmony import */ var _about_toko_about_toko_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./about-toko/about-toko.component */ "./src/app/about-toko/about-toko.component.ts");














let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _register_register_component__WEBPACK_IMPORTED_MODULE_6__["RegisterComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"],
            _metode_pembayaran_metode_pembayaran_component__WEBPACK_IMPORTED_MODULE_8__["MetodePembayaranComponent"],
            _support_support_component__WEBPACK_IMPORTED_MODULE_9__["SupportComponent"],
            _profile_toko_profile_toko_component__WEBPACK_IMPORTED_MODULE_10__["ProfileTokoComponent"],
            _produk_ptoko_produk_ptoko_component__WEBPACK_IMPORTED_MODULE_11__["ProdukPTokoComponent"],
            _ulasan_toko_ulasan_toko_component__WEBPACK_IMPORTED_MODULE_12__["UlasanTokoComponent"],
            _about_toko_about_toko_component__WEBPACK_IMPORTED_MODULE_13__["AboutTokoComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("body {\n  margin: 0px;\n  padding: 0px;\n}\nbody p, body h1, body h2, body h2, body h3, body h4, body h5, body h6 {\n  margin: 0px;\n}\n.wrapper .row .bagian1 {\n  background-color: #39dbcd;\n  padding: 10px;\n}\n.wrapper .row .bagian1 .wrapper1 {\n  padding-top: 2em;\n  margin: 0em 2em 0em 2em;\n}\n.wrapper .row .bagian1 .wrapper1 .logo #logoLoutaro {\n  width: 172px;\n}\n.wrapper .row .bagian1 .wrapper1 .textSlogan {\n  margin-top: 2em;\n}\n.wrapper .row .bagian2 {\n  padding: 20px;\n}\n.wrapper .row .bagian2 .atas {\n  margin-top: 5em;\n}\n.wrapper .row .bagian2 .atas .bagianatas .logob #logoLoutaro {\n  width: 100px;\n  margin: 1em 0em;\n  display: none;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial {\n  margin: 1em 0em;\n  height: 60px;\n  padding: 10px 0px 10px 0px;\n  display: flex;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial img {\n  width: 30px;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial .google {\n  padding: 5px;\n  background-color: #39dbcd;\n  display: flex;\n  border-radius: 5px;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial .google p {\n  padding: 4px;\n  font-size: 12px;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial .google:hover {\n  background-color: #39a89f;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial .facebook {\n  border-radius: 5px;\n  margin-left: 1em;\n  padding: 5px;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial .facebook:hover {\n  background-color: #39a89f;\n}\n.wrapper .row .bagian2 .atas .pemisah {\n  margin: 1em 0em;\n}\n.wrapper .row .bagian2 .atas .bagianBawah {\n  margin-top: 1em;\n}\n@media only screen and (max-width: 767.98px) {\n  .wrapper .row .bagian1 {\n    display: none;\n  }\n  .wrapper .row .bagian2 .atas .bagianatas .logob #logoLoutaro {\n    display: block;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tZWRpYS9mYXJoYW5hcnlhL0RhdGEvcHJveWVrL3BrdGkxL0xvZ2luUjEvc3JjL2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ0RKO0FERUk7RUFDSSxXQUFBO0FDQVI7QURNUTtFQUNJLHlCQUFBO0VBQ0EsYUFBQTtBQ0haO0FESVk7RUFDSSxnQkFBQTtFQUNBLHVCQUFBO0FDRmhCO0FESW9CO0VBQ0ksWUFBQTtBQ0Z4QjtBREtnQjtFQUNJLGVBQUE7QUNIcEI7QURRUTtFQUNJLGFBQUE7QUNOWjtBRE9ZO0VBQ0ksZUFBQTtBQ0xoQjtBRFF3QjtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtBQ041QjtBRFNvQjtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0FDUHhCO0FEU3dCO0VBQ0ksV0FBQTtBQ1A1QjtBRFl3QjtFQUNJLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGFBQUE7RUFRQSxrQkFBQTtBQ2pCNUI7QURZNEI7RUFDSSxZQUFBO0VBQ0EsZUFBQTtBQ1ZoQztBRGV3QjtFQUNJLHlCQUFBO0FDYjVCO0FEZXdCO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUNiNUI7QURld0I7RUFDSSx5QkFBQTtBQ2I1QjtBRGtCZ0I7RUFDSSxlQUFBO0FDaEJwQjtBRG1CZ0I7RUFDSSxlQUFBO0FDakJwQjtBRDBCQTtFQUdRO0lBQ0ksYUFBQTtFQ3pCVjtFRCtCc0I7SUFDSSxjQUFBO0VDN0IxQjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvL2JhbWJhbmdcblxuYm9keXtcbiAgICBtYXJnaW46IDBweDtcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgcCxoMSxoMixoMixoMyxoNCxoNSxoNiB7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgIH1cbn1cblxuLndyYXBwZXJ7XG4gICAgLnJvd3tcbiAgICAgICAgLmJhZ2lhbjF7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiMzOWRiY2Q7XG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICAgICAgLndyYXBwZXIxe1xuICAgICAgICAgICAgICAgIHBhZGRpbmctdG9wOiAyZW07XG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwZW0gMmVtIDBlbSAyZW07XG4gICAgICAgICAgICAgICAgLmxvZ297XG4gICAgICAgICAgICAgICAgICAgICNsb2dvTG91dGFyb3tcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxNzJweDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAudGV4dFNsb2dhbntcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMmVtO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5iYWdpYW4ye1xuICAgICAgICAgICAgcGFkZGluZzogMjBweDtcbiAgICAgICAgICAgIC5hdGFze1xuICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDVlbTtcbiAgICAgICAgICAgICAgICAuYmFnaWFuYXRhc3tcbiAgICAgICAgICAgICAgICAgICAgLmxvZ29ie1xuICAgICAgICAgICAgICAgICAgICAgICAgI2xvZ29Mb3V0YXJve1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDBweDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDFlbSAwZW07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC5tZWRpYXNvc2lhbHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMWVtIDBlbTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNjBweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDEwcHggMHB4IDEwcHggMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgaW1ne1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgIC5nb29nbGV7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogNXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzOWRiY2QgO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogNHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAuZ29vZ2xlOmhvdmVye1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IzM5YTg5ZiA7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAuZmFjZWJvb2t7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxZW07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogNXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgLmZhY2Vib29rOmhvdmVye1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IzM5YTg5ZiA7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAucGVtaXNhaHtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAxZW0gMGVtO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC5iYWdpYW5CYXdhaHtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMWVtO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XG5cbiAgICB9XG59XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3Ljk4cHgpIHsgXG4ud3JhcHBlcntcbiAgICAucm93e1xuICAgICAgICAuYmFnaWFuMXtcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICAgIH1cbiAgICAgICAgLmJhZ2lhbjJ7XG4gICAgICAgICAgICAuYXRhc3tcbiAgICAgICAgICAgICAgICAuYmFnaWFuYXRhc3tcbiAgICAgICAgICAgICAgICAgICAgLmxvZ29ie1xuICAgICAgICAgICAgICAgICAgICAgICAgI2xvZ29Mb3V0YXJve1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgIH1cbn1cbiAgICBcblxufSIsImJvZHkge1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMHB4O1xufVxuYm9keSBwLCBib2R5IGgxLCBib2R5IGgyLCBib2R5IGgyLCBib2R5IGgzLCBib2R5IGg0LCBib2R5IGg1LCBib2R5IGg2IHtcbiAgbWFyZ2luOiAwcHg7XG59XG5cbi53cmFwcGVyIC5yb3cgLmJhZ2lhbjEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzlkYmNkO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuLndyYXBwZXIgLnJvdyAuYmFnaWFuMSAud3JhcHBlcjEge1xuICBwYWRkaW5nLXRvcDogMmVtO1xuICBtYXJnaW46IDBlbSAyZW0gMGVtIDJlbTtcbn1cbi53cmFwcGVyIC5yb3cgLmJhZ2lhbjEgLndyYXBwZXIxIC5sb2dvICNsb2dvTG91dGFybyB7XG4gIHdpZHRoOiAxNzJweDtcbn1cbi53cmFwcGVyIC5yb3cgLmJhZ2lhbjEgLndyYXBwZXIxIC50ZXh0U2xvZ2FuIHtcbiAgbWFyZ2luLXRvcDogMmVtO1xufVxuLndyYXBwZXIgLnJvdyAuYmFnaWFuMiB7XG4gIHBhZGRpbmc6IDIwcHg7XG59XG4ud3JhcHBlciAucm93IC5iYWdpYW4yIC5hdGFzIHtcbiAgbWFyZ2luLXRvcDogNWVtO1xufVxuLndyYXBwZXIgLnJvdyAuYmFnaWFuMiAuYXRhcyAuYmFnaWFuYXRhcyAubG9nb2IgI2xvZ29Mb3V0YXJvIHtcbiAgd2lkdGg6IDEwMHB4O1xuICBtYXJnaW46IDFlbSAwZW07XG4gIGRpc3BsYXk6IG5vbmU7XG59XG4ud3JhcHBlciAucm93IC5iYWdpYW4yIC5hdGFzIC5iYWdpYW5hdGFzIC5tZWRpYXNvc2lhbCB7XG4gIG1hcmdpbjogMWVtIDBlbTtcbiAgaGVpZ2h0OiA2MHB4O1xuICBwYWRkaW5nOiAxMHB4IDBweCAxMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbn1cbi53cmFwcGVyIC5yb3cgLmJhZ2lhbjIgLmF0YXMgLmJhZ2lhbmF0YXMgLm1lZGlhc29zaWFsIGltZyB7XG4gIHdpZHRoOiAzMHB4O1xufVxuLndyYXBwZXIgLnJvdyAuYmFnaWFuMiAuYXRhcyAuYmFnaWFuYXRhcyAubWVkaWFzb3NpYWwgLmdvb2dsZSB7XG4gIHBhZGRpbmc6IDVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM5ZGJjZDtcbiAgZGlzcGxheTogZmxleDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLndyYXBwZXIgLnJvdyAuYmFnaWFuMiAuYXRhcyAuYmFnaWFuYXRhcyAubWVkaWFzb3NpYWwgLmdvb2dsZSBwIHtcbiAgcGFkZGluZzogNHB4O1xuICBmb250LXNpemU6IDEycHg7XG59XG4ud3JhcHBlciAucm93IC5iYWdpYW4yIC5hdGFzIC5iYWdpYW5hdGFzIC5tZWRpYXNvc2lhbCAuZ29vZ2xlOmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM5YTg5Zjtcbn1cbi53cmFwcGVyIC5yb3cgLmJhZ2lhbjIgLmF0YXMgLmJhZ2lhbmF0YXMgLm1lZGlhc29zaWFsIC5mYWNlYm9vayB7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgbWFyZ2luLWxlZnQ6IDFlbTtcbiAgcGFkZGluZzogNXB4O1xufVxuLndyYXBwZXIgLnJvdyAuYmFnaWFuMiAuYXRhcyAuYmFnaWFuYXRhcyAubWVkaWFzb3NpYWwgLmZhY2Vib29rOmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM5YTg5Zjtcbn1cbi53cmFwcGVyIC5yb3cgLmJhZ2lhbjIgLmF0YXMgLnBlbWlzYWgge1xuICBtYXJnaW46IDFlbSAwZW07XG59XG4ud3JhcHBlciAucm93IC5iYWdpYW4yIC5hdGFzIC5iYWdpYW5CYXdhaCB7XG4gIG1hcmdpbi10b3A6IDFlbTtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjcuOThweCkge1xuICAud3JhcHBlciAucm93IC5iYWdpYW4xIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG4gIC53cmFwcGVyIC5yb3cgLmJhZ2lhbjIgLmF0YXMgLmJhZ2lhbmF0YXMgLmxvZ29iICNsb2dvTG91dGFybyB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cbn0iXX0= */");

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let LoginComponent = class LoginComponent {
    constructor() { }
    ngOnInit() {
    }
};
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")).default]
    })
], LoginComponent);



/***/ }),

/***/ "./src/app/metode-pembayaran/metode-pembayaran.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/metode-pembayaran/metode-pembayaran.component.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container p {\n  font-size: 12px;\n  margin: 0px;\n}\n.container h1, .container h2, .container h3, .container h4, .container h5, .container h6 {\n  margin: 0px;\n}\n.container .wrapper {\n  margin-top: 2em;\n}\n.container .wrapper .bagian1 .detailPembayaran {\n  border: 1px solid black;\n  padding: 10px 15px;\n}\n.container .wrapper .bagian1 .detailPembayaran .borderCatatan {\n  margin-top: 1em;\n  border-left: 6px solid #39dbcd;\n  background-color: lightgrey;\n  display: flex;\n  height: 45px;\n}\n.container .wrapper .bagian1 .detailPembayaran .borderCatatan .iconCart {\n  align-self: center;\n  padding: 0px 5px;\n}\n.container .wrapper .bagian1 .detailPembayaran .borderCatatan .iconCart img {\n  width: 20px;\n}\n.container .wrapper .bagian1 .detailPembayaran .borderCatatan .catatan {\n  align-self: center;\n  padding-left: 5px;\n}\n.container .wrapper .bagian1 .detailPembayaran .identitas {\n  margin-top: 1em;\n}\n.container .wrapper .bagian1 .detailPembayaran .identitas #kodePembayaran {\n  margin-top: 1em;\n}\n.container .wrapper .bagian1 .metodePembayaran {\n  margin-top: 1em;\n  border: 1px solid black;\n  padding: 10px 15px;\n}\n.container .wrapper .bagian1 .metodePembayaran .metodePText {\n  align-self: center;\n}\n.container .wrapper .bagian1 .metodePembayaran .Emoney img {\n  width: 50px;\n  padding: 5px;\n  cursor: pointer;\n}\n.container .wrapper .bagian2 .pembayaran {\n  border: 1px solid black;\n  padding: 10px 15px;\n}\n.container .wrapper .bagian2 .pembayaran .garis {\n  border-bottom: black;\n}\n.container .wrapper .bagian2 .pembayaran .listHarga .table {\n  margin: 0px;\n}\n.container .wrapper .bagian2 .pembayaran .listHarga .table td, .container .wrapper .bagian2 .pembayaran .listHarga .table th {\n  border: none;\n}\n.container .wrapper .bagian2 .pembayaran .totalHarga {\n  margin-top: 1em;\n}\n.container .wrapper .bagian2 .pembayaran .totalHarga .table {\n  margin: 0px;\n}\n.container .wrapper .bagian2 .pembayaran #beli {\n  margin-top: 1em;\n}\n@media (max-width: 991.98px) {\n  .bagian2 {\n    margin-top: 1em;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tZWRpYS9mYXJoYW5hcnlhL0RhdGEvcHJveWVrL3BrdGkxL0xvZ2luUjEvc3JjL2FwcC9tZXRvZGUtcGVtYmF5YXJhbi9tZXRvZGUtcGVtYmF5YXJhbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbWV0b2RlLXBlbWJheWFyYW4vbWV0b2RlLXBlbWJheWFyYW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxlQUFBO0VBQ0EsV0FBQTtBQ0FSO0FERUk7RUFDSSxXQUFBO0FDQVI7QURFSTtFQUNJLGVBQUE7QUNBUjtBREdZO0VBQ0ksdUJBQUE7RUFDQSxrQkFBQTtBQ0RoQjtBREVnQjtFQUNJLGVBQUE7RUFDQSw4QkFBQTtFQUNBLDJCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7QUNBcEI7QURDb0I7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0FDQ3hCO0FEQXdCO0VBQ0ksV0FBQTtBQ0U1QjtBRENvQjtFQUNJLGtCQUFBO0VBQ0EsaUJBQUE7QUNDeEI7QURHZ0I7RUFDSSxlQUFBO0FDRHBCO0FERW9CO0VBQ0ksZUFBQTtBQ0F4QjtBRE1ZO0VBQ0ksZUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7QUNKaEI7QURLZ0I7RUFDSSxrQkFBQTtBQ0hwQjtBRE1vQjtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQ0p4QjtBRGFZO0VBQ0ksdUJBQUE7RUFDQSxrQkFBQTtBQ1hoQjtBRFlnQjtFQUNJLG9CQUFBO0FDVnBCO0FEYW9CO0VBQ0ksV0FBQTtBQ1h4QjtBRFl3QjtFQUNJLFlBQUE7QUNWNUI7QURnQmdCO0VBQ0ksZUFBQTtBQ2RwQjtBRGVvQjtFQUNJLFdBQUE7QUNieEI7QURnQmdCO0VBQ0ksZUFBQTtBQ2RwQjtBRHVCQTtFQUNJO0lBQ0ksZUFBQTtFQ3BCTjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvbWV0b2RlLXBlbWJheWFyYW4vbWV0b2RlLXBlbWJheWFyYW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVye1xuICAgIHAge1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgIH1cbiAgICBoMSxoMixoMyxoNCxoNSxoNntcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgfVxuICAgIC53cmFwcGVye1xuICAgICAgICBtYXJnaW4tdG9wOiAyZW07XG5cbiAgICAgICAgLmJhZ2lhbjF7XG4gICAgICAgICAgICAuZGV0YWlsUGVtYmF5YXJhbntcbiAgICAgICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCBibGFjaztcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAxMHB4IDE1cHg7XG4gICAgICAgICAgICAgICAgLmJvcmRlckNhdGF0YW57XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDFlbTtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDZweCBzb2xpZCAjMzlkYmNkO1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyZXk7XG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDVweDtcbiAgICAgICAgICAgICAgICAgICAgLmljb25DYXJ0e1xuICAgICAgICAgICAgICAgICAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzowcHggNXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgaW1ne1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC5jYXRhdGFue1xuICAgICAgICAgICAgICAgICAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAuaWRlbnRpdGFze1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxZW07XG4gICAgICAgICAgICAgICAgICAgICNrb2RlUGVtYmF5YXJhbntcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDFlbTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAubWV0b2RlUGVtYmF5YXJhbntcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxZW07XG4gICAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogMTBweCAxNXB4O1xuICAgICAgICAgICAgICAgIC5tZXRvZGVQVGV4dHtcbiAgICAgICAgICAgICAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAuRW1vbmV5e1xuICAgICAgICAgICAgICAgICAgICBpbWd7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogNTBweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgIH1cblxuICAgICAgICAuYmFnaWFuMntcbiAgICAgICAgICAgIC5wZW1iYXlhcmFue1xuICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrO1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDEwcHggMTVweDtcbiAgICAgICAgICAgICAgICAuZ2FyaXN7XG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b206YmxhY2s7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5saXN0SGFyZ2F7XG4gICAgICAgICAgICAgICAgICAgIC50YWJsZXtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgdGQsIHRoIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC50b3RhbEhhcmdhe1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxZW07XG4gICAgICAgICAgICAgICAgICAgIC50YWJsZXtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICNiZWxpe1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxZW07XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgIH1cbiAgICB9XG4gICAgXG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA5OTEuOThweCl7XG4gICAgLmJhZ2lhbjJ7XG4gICAgICAgIG1hcmdpbi10b3A6IDFlbTtcbiAgICB9XG59IiwiLmNvbnRhaW5lciBwIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBtYXJnaW46IDBweDtcbn1cbi5jb250YWluZXIgaDEsIC5jb250YWluZXIgaDIsIC5jb250YWluZXIgaDMsIC5jb250YWluZXIgaDQsIC5jb250YWluZXIgaDUsIC5jb250YWluZXIgaDYge1xuICBtYXJnaW46IDBweDtcbn1cbi5jb250YWluZXIgLndyYXBwZXIge1xuICBtYXJnaW4tdG9wOiAyZW07XG59XG4uY29udGFpbmVyIC53cmFwcGVyIC5iYWdpYW4xIC5kZXRhaWxQZW1iYXlhcmFuIHtcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XG4gIHBhZGRpbmc6IDEwcHggMTVweDtcbn1cbi5jb250YWluZXIgLndyYXBwZXIgLmJhZ2lhbjEgLmRldGFpbFBlbWJheWFyYW4gLmJvcmRlckNhdGF0YW4ge1xuICBtYXJnaW4tdG9wOiAxZW07XG4gIGJvcmRlci1sZWZ0OiA2cHggc29saWQgIzM5ZGJjZDtcbiAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmV5O1xuICBkaXNwbGF5OiBmbGV4O1xuICBoZWlnaHQ6IDQ1cHg7XG59XG4uY29udGFpbmVyIC53cmFwcGVyIC5iYWdpYW4xIC5kZXRhaWxQZW1iYXlhcmFuIC5ib3JkZXJDYXRhdGFuIC5pY29uQ2FydCB7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgcGFkZGluZzogMHB4IDVweDtcbn1cbi5jb250YWluZXIgLndyYXBwZXIgLmJhZ2lhbjEgLmRldGFpbFBlbWJheWFyYW4gLmJvcmRlckNhdGF0YW4gLmljb25DYXJ0IGltZyB7XG4gIHdpZHRoOiAyMHB4O1xufVxuLmNvbnRhaW5lciAud3JhcHBlciAuYmFnaWFuMSAuZGV0YWlsUGVtYmF5YXJhbiAuYm9yZGVyQ2F0YXRhbiAuY2F0YXRhbiB7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG59XG4uY29udGFpbmVyIC53cmFwcGVyIC5iYWdpYW4xIC5kZXRhaWxQZW1iYXlhcmFuIC5pZGVudGl0YXMge1xuICBtYXJnaW4tdG9wOiAxZW07XG59XG4uY29udGFpbmVyIC53cmFwcGVyIC5iYWdpYW4xIC5kZXRhaWxQZW1iYXlhcmFuIC5pZGVudGl0YXMgI2tvZGVQZW1iYXlhcmFuIHtcbiAgbWFyZ2luLXRvcDogMWVtO1xufVxuLmNvbnRhaW5lciAud3JhcHBlciAuYmFnaWFuMSAubWV0b2RlUGVtYmF5YXJhbiB7XG4gIG1hcmdpbi10b3A6IDFlbTtcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XG4gIHBhZGRpbmc6IDEwcHggMTVweDtcbn1cbi5jb250YWluZXIgLndyYXBwZXIgLmJhZ2lhbjEgLm1ldG9kZVBlbWJheWFyYW4gLm1ldG9kZVBUZXh0IHtcbiAgYWxpZ24tc2VsZjogY2VudGVyO1xufVxuLmNvbnRhaW5lciAud3JhcHBlciAuYmFnaWFuMSAubWV0b2RlUGVtYmF5YXJhbiAuRW1vbmV5IGltZyB7XG4gIHdpZHRoOiA1MHB4O1xuICBwYWRkaW5nOiA1cHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5jb250YWluZXIgLndyYXBwZXIgLmJhZ2lhbjIgLnBlbWJheWFyYW4ge1xuICBib3JkZXI6IDFweCBzb2xpZCBibGFjaztcbiAgcGFkZGluZzogMTBweCAxNXB4O1xufVxuLmNvbnRhaW5lciAud3JhcHBlciAuYmFnaWFuMiAucGVtYmF5YXJhbiAuZ2FyaXMge1xuICBib3JkZXItYm90dG9tOiBibGFjaztcbn1cbi5jb250YWluZXIgLndyYXBwZXIgLmJhZ2lhbjIgLnBlbWJheWFyYW4gLmxpc3RIYXJnYSAudGFibGUge1xuICBtYXJnaW46IDBweDtcbn1cbi5jb250YWluZXIgLndyYXBwZXIgLmJhZ2lhbjIgLnBlbWJheWFyYW4gLmxpc3RIYXJnYSAudGFibGUgdGQsIC5jb250YWluZXIgLndyYXBwZXIgLmJhZ2lhbjIgLnBlbWJheWFyYW4gLmxpc3RIYXJnYSAudGFibGUgdGgge1xuICBib3JkZXI6IG5vbmU7XG59XG4uY29udGFpbmVyIC53cmFwcGVyIC5iYWdpYW4yIC5wZW1iYXlhcmFuIC50b3RhbEhhcmdhIHtcbiAgbWFyZ2luLXRvcDogMWVtO1xufVxuLmNvbnRhaW5lciAud3JhcHBlciAuYmFnaWFuMiAucGVtYmF5YXJhbiAudG90YWxIYXJnYSAudGFibGUge1xuICBtYXJnaW46IDBweDtcbn1cbi5jb250YWluZXIgLndyYXBwZXIgLmJhZ2lhbjIgLnBlbWJheWFyYW4gI2JlbGkge1xuICBtYXJnaW4tdG9wOiAxZW07XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA5OTEuOThweCkge1xuICAuYmFnaWFuMiB7XG4gICAgbWFyZ2luLXRvcDogMWVtO1xuICB9XG59Il19 */");

/***/ }),

/***/ "./src/app/metode-pembayaran/metode-pembayaran.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/metode-pembayaran/metode-pembayaran.component.ts ***!
  \******************************************************************/
/*! exports provided: MetodePembayaranComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MetodePembayaranComponent", function() { return MetodePembayaranComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let MetodePembayaranComponent = class MetodePembayaranComponent {
    constructor() { }
    ngOnInit() {
    }
};
MetodePembayaranComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-metode-pembayaran',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./metode-pembayaran.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/metode-pembayaran/metode-pembayaran.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./metode-pembayaran.component.scss */ "./src/app/metode-pembayaran/metode-pembayaran.component.scss")).default]
    })
], MetodePembayaranComponent);



/***/ }),

/***/ "./src/app/produk-ptoko/produk-ptoko.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/produk-ptoko/produk-ptoko.component.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wrapper {\n  background-color: #FAFAFA;\n  margin-top: 2em;\n}\n.wrapper p {\n  font-size: 20px;\n  margin: 0px;\n}\n.wrapper h1, .wrapper h2, .wrapper h3, .wrapper h4, .wrapper h5, .wrapper h6 {\n  margin: 0px;\n}\n.wrapper .container .kategori {\n  margin-top: 2em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tZWRpYS9mYXJoYW5hcnlhL0RhdGEvcHJveWVrL3BrdGkxL0xvZ2luUjEvc3JjL2FwcC9wcm9kdWstcHRva28vcHJvZHVrLXB0b2tvLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wcm9kdWstcHRva28vcHJvZHVrLXB0b2tvLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFRQSxlQUFBO0FDTko7QURESTtFQUNJLGVBQUE7RUFDQSxXQUFBO0FDR1I7QURESTtFQUNJLFdBQUE7QUNHUjtBRENRO0VBQ0ksZUFBQTtBQ0NaIiwiZmlsZSI6InNyYy9hcHAvcHJvZHVrLXB0b2tvL3Byb2R1ay1wdG9rby5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi53cmFwcGVye1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGQUZBRkE7XG4gICAgcCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgfVxuICAgIGgxLGgyLGgzLGg0LGg1LGg2e1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICB9XG4gICAgbWFyZ2luLXRvcDogMmVtO1xuICAgIC5jb250YWluZXJ7XG4gICAgICAgIC5rYXRlZ29yaXtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDJlbTtcbiAgICAgICAgfVxuXG4gICAgfVxufSIsIi53cmFwcGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZBRkFGQTtcbiAgbWFyZ2luLXRvcDogMmVtO1xufVxuLndyYXBwZXIgcCB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbWFyZ2luOiAwcHg7XG59XG4ud3JhcHBlciBoMSwgLndyYXBwZXIgaDIsIC53cmFwcGVyIGgzLCAud3JhcHBlciBoNCwgLndyYXBwZXIgaDUsIC53cmFwcGVyIGg2IHtcbiAgbWFyZ2luOiAwcHg7XG59XG4ud3JhcHBlciAuY29udGFpbmVyIC5rYXRlZ29yaSB7XG4gIG1hcmdpbi10b3A6IDJlbTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/produk-ptoko/produk-ptoko.component.ts":
/*!********************************************************!*\
  !*** ./src/app/produk-ptoko/produk-ptoko.component.ts ***!
  \********************************************************/
/*! exports provided: ProdukPTokoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProdukPTokoComponent", function() { return ProdukPTokoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ProdukPTokoComponent = class ProdukPTokoComponent {
    constructor() { }
    ngOnInit() {
    }
};
ProdukPTokoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-produk-ptoko',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./produk-ptoko.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/produk-ptoko/produk-ptoko.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./produk-ptoko.component.scss */ "./src/app/produk-ptoko/produk-ptoko.component.scss")).default]
    })
], ProdukPTokoComponent);



/***/ }),

/***/ "./src/app/profile-toko/profile-toko.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/profile-toko/profile-toko.component.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wrapper {\n  margin-top: 2em;\n}\n.wrapper p {\n  font-size: 20px;\n  margin: 0px;\n}\n.wrapper h1, .wrapper h2, .wrapper h3, .wrapper h4, .wrapper h5, .wrapper h6 {\n  margin: 0px;\n}\n.wrapper .container-fluid {\n  width: 70%;\n}\n.wrapper .container-fluid .headToko .headProfil .picture {\n  padding: 10px;\n  width: 140px;\n}\n.wrapper .container-fluid .headToko .headProfil .picture .ava {\n  width: 150px;\n  border: 2px solid gray;\n}\n.wrapper .container-fluid .headToko .headProfil .desc .desc1 {\n  margin-top: 10px;\n}\n.wrapper .container-fluid .garis {\n  margin: 2em 0em;\n}\n.wrapper .container-fluid .navbar {\n  width: 100%;\n}\n.wrapper .container-fluid .navbar .produk a {\n  border-top: 1px solid gray;\n}\n.wrapper .container-fluid .navbar a {\n  cursor: pointer;\n  padding-top: 10px;\n  color: black;\n  text-decoration: none;\n}\n.wrapper .container-fluid .navbar a:hover {\n  border-top: 1px solid gray;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tZWRpYS9mYXJoYW5hcnlhL0RhdGEvcHJveWVrL3BrdGkxL0xvZ2luUjEvc3JjL2FwcC9wcm9maWxlLXRva28vcHJvZmlsZS10b2tvLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wcm9maWxlLXRva28vcHJvZmlsZS10b2tvLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBUUksZUFBQTtBQ05KO0FEREk7RUFDSSxlQUFBO0VBQ0EsV0FBQTtBQ0dSO0FEREk7RUFDSSxXQUFBO0FDR1I7QURDSTtFQUNJLFVBQUE7QUNDUjtBREVnQjtFQUNJLGFBQUE7RUFDQSxZQUFBO0FDQXBCO0FEQ29CO0VBQ0ksWUFBQTtFQUNBLHNCQUFBO0FDQ3hCO0FER29CO0VBQ0ksZ0JBQUE7QUNEeEI7QURNUTtFQUNJLGVBQUE7QUNKWjtBRE1RO0VBQ0ksV0FBQTtBQ0paO0FETWdCO0VBQ0ksMEJBQUE7QUNKcEI7QURPWTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtBQ0xoQjtBRFFZO0VBQ0ksMEJBQUE7QUNOaEIiLCJmaWxlIjoic3JjL2FwcC9wcm9maWxlLXRva28vcHJvZmlsZS10b2tvLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndyYXBwZXJ7XG4gICAgcCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgfVxuICAgIGgxLGgyLGgzLGg0LGg1LGg2e1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICB9XG4gICAgbWFyZ2luLXRvcDogMmVtO1xuXG4gICAgLmNvbnRhaW5lci1mbHVpZHtcbiAgICAgICAgd2lkdGg6NzAlOyAgICAgICAgXG4gICAgICAgIC5oZWFkVG9rb3tcbiAgICAgICAgICAgIC5oZWFkUHJvZmlse1xuICAgICAgICAgICAgICAgIC5waWN0dXJle1xuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTQwcHg7XG4gICAgICAgICAgICAgICAgICAgIC5hdmF7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTUwcHg7XG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXI6IDJweCBzb2xpZCBncmF5O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5kZXNje1xuICAgICAgICAgICAgICAgICAgICAuZGVzYzF7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5nYXJpc3tcbiAgICAgICAgICAgIG1hcmdpbjoyZW0gMGVtIDtcbiAgICAgICAgfVxuICAgICAgICAubmF2YmFye1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAucHJvZHVre1xuICAgICAgICAgICAgICAgIGF7XG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCBncmF5O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGF7XG4gICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICAgICAgICAgIGNvbG9yOiBibGFjaztcbiAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBhOmhvdmVye1xuICAgICAgICAgICAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCBncmF5O1xuICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9XG59IiwiLndyYXBwZXIge1xuICBtYXJnaW4tdG9wOiAyZW07XG59XG4ud3JhcHBlciBwIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBtYXJnaW46IDBweDtcbn1cbi53cmFwcGVyIGgxLCAud3JhcHBlciBoMiwgLndyYXBwZXIgaDMsIC53cmFwcGVyIGg0LCAud3JhcHBlciBoNSwgLndyYXBwZXIgaDYge1xuICBtYXJnaW46IDBweDtcbn1cbi53cmFwcGVyIC5jb250YWluZXItZmx1aWQge1xuICB3aWR0aDogNzAlO1xufVxuLndyYXBwZXIgLmNvbnRhaW5lci1mbHVpZCAuaGVhZFRva28gLmhlYWRQcm9maWwgLnBpY3R1cmUge1xuICBwYWRkaW5nOiAxMHB4O1xuICB3aWR0aDogMTQwcHg7XG59XG4ud3JhcHBlciAuY29udGFpbmVyLWZsdWlkIC5oZWFkVG9rbyAuaGVhZFByb2ZpbCAucGljdHVyZSAuYXZhIHtcbiAgd2lkdGg6IDE1MHB4O1xuICBib3JkZXI6IDJweCBzb2xpZCBncmF5O1xufVxuLndyYXBwZXIgLmNvbnRhaW5lci1mbHVpZCAuaGVhZFRva28gLmhlYWRQcm9maWwgLmRlc2MgLmRlc2MxIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi53cmFwcGVyIC5jb250YWluZXItZmx1aWQgLmdhcmlzIHtcbiAgbWFyZ2luOiAyZW0gMGVtO1xufVxuLndyYXBwZXIgLmNvbnRhaW5lci1mbHVpZCAubmF2YmFyIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ud3JhcHBlciAuY29udGFpbmVyLWZsdWlkIC5uYXZiYXIgLnByb2R1ayBhIHtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGdyYXk7XG59XG4ud3JhcHBlciAuY29udGFpbmVyLWZsdWlkIC5uYXZiYXIgYSB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIGNvbG9yOiBibGFjaztcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuLndyYXBwZXIgLmNvbnRhaW5lci1mbHVpZCAubmF2YmFyIGE6aG92ZXIge1xuICBib3JkZXItdG9wOiAxcHggc29saWQgZ3JheTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/profile-toko/profile-toko.component.ts":
/*!********************************************************!*\
  !*** ./src/app/profile-toko/profile-toko.component.ts ***!
  \********************************************************/
/*! exports provided: ProfileTokoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileTokoComponent", function() { return ProfileTokoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ProfileTokoComponent = class ProfileTokoComponent {
    constructor() { }
    ngOnInit() {
    }
};
ProfileTokoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile-toko',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./profile-toko.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile-toko/profile-toko.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./profile-toko.component.scss */ "./src/app/profile-toko/profile-toko.component.scss")).default]
    })
], ProfileTokoComponent);



/***/ }),

/***/ "./src/app/register/register.component.scss":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("body {\n  margin: 0px;\n  padding: 0px;\n}\nbody p, body h1, body h2, body h2, body h3, body h4, body h5, body h6 {\n  margin: 0px;\n}\n.wrapper .row .bagian1 {\n  background-color: #51e0d4;\n  padding: 10px;\n}\n.wrapper .row .bagian1 .wrapper1 {\n  padding-top: 2em;\n  margin: 0em 2em 0em 2em;\n}\n.wrapper .row .bagian1 .wrapper1 .logo #logoLoutaro {\n  width: 172px;\n}\n.wrapper .row .bagian1 .wrapper1 .textSlogan {\n  margin-top: 2em;\n}\n.wrapper .row .bagian2 {\n  padding: 20px;\n}\n.wrapper .row .bagian2 .atas {\n  margin-top: 3em;\n}\n.wrapper .row .bagian2 .atas .bagianatas .logob #logoLoutaro {\n  width: 100px;\n  margin: 1em 0em;\n  display: none;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial {\n  margin: 1em 0em;\n  height: 60px;\n  padding: 10px 0px 10px 0px;\n  display: flex;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial img {\n  width: 30px;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial .google {\n  padding: 5px;\n  background-color: #51e0d4;\n  display: flex;\n  border-radius: 5px;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial .google p {\n  padding: 4px;\n  font-size: 12px;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial .google:hover {\n  background-color: #39a89f;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial .facebook {\n  border-radius: 5px;\n  margin-left: 1em;\n  padding: 5px;\n}\n.wrapper .row .bagian2 .atas .bagianatas .mediasosial .facebook:hover {\n  background-color: #39a89f;\n}\n.wrapper .row .bagian2 .atas .pemisah {\n  margin: 1em 0em;\n}\n.wrapper .row .bagian2 .atas #btnAkunB {\n  margin-top: 1em;\n}\n@media only screen and (max-width: 767.98px) {\n  .wrapper .row .bagian1 {\n    display: none;\n  }\n  .wrapper .row .bagian2 .atas .bagianatas .logob #logoLoutaro {\n    display: block;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tZWRpYS9mYXJoYW5hcnlhL0RhdGEvcHJveWVrL3BrdGkxL0xvZ2luUjEvc3JjL2FwcC9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ0RKO0FERUk7RUFDSSxXQUFBO0FDQVI7QURNUTtFQUNJLHlCQUFBO0VBQ0EsYUFBQTtBQ0haO0FESVk7RUFDSSxnQkFBQTtFQUNBLHVCQUFBO0FDRmhCO0FESW9CO0VBQ0ksWUFBQTtBQ0Z4QjtBREtnQjtFQUNJLGVBQUE7QUNIcEI7QURRUTtFQUNJLGFBQUE7QUNOWjtBRE9ZO0VBQ0ksZUFBQTtBQ0xoQjtBRFF3QjtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtBQ041QjtBRFNvQjtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0FDUHhCO0FEU3dCO0VBQ0ksV0FBQTtBQ1A1QjtBRFl3QjtFQUNJLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGFBQUE7RUFRQSxrQkFBQTtBQ2pCNUI7QURZNEI7RUFDSSxZQUFBO0VBQ0EsZUFBQTtBQ1ZoQztBRGV3QjtFQUNJLHlCQUFBO0FDYjVCO0FEZXdCO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUNiNUI7QURld0I7RUFDSSx5QkFBQTtBQ2I1QjtBRGtCZ0I7RUFDSSxlQUFBO0FDaEJwQjtBRG1CZ0I7RUFDSSxlQUFBO0FDakJwQjtBRDBCQTtFQUdRO0lBQ0ksYUFBQTtFQ3pCVjtFRCtCc0I7SUFDSSxjQUFBO0VDN0IxQjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvL2JhbWJhbmdcblxuYm9keXtcbiAgICBtYXJnaW46IDBweDtcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgcCxoMSxoMixoMixoMyxoNCxoNSxoNiB7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgIH1cbn1cblxuLndyYXBwZXJ7XG4gICAgLnJvd3tcbiAgICAgICAgLmJhZ2lhbjF7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiM1MWUwZDQ7XG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICAgICAgLndyYXBwZXIxe1xuICAgICAgICAgICAgICAgIHBhZGRpbmctdG9wOiAyZW07XG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwZW0gMmVtIDBlbSAyZW07XG4gICAgICAgICAgICAgICAgLmxvZ297XG4gICAgICAgICAgICAgICAgICAgICNsb2dvTG91dGFyb3tcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxNzJweDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAudGV4dFNsb2dhbntcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMmVtO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5iYWdpYW4ye1xuICAgICAgICAgICAgcGFkZGluZzogMjBweDtcbiAgICAgICAgICAgIC5hdGFze1xuICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDNlbTtcbiAgICAgICAgICAgICAgICAuYmFnaWFuYXRhc3tcbiAgICAgICAgICAgICAgICAgICAgLmxvZ29ie1xuICAgICAgICAgICAgICAgICAgICAgICAgI2xvZ29Mb3V0YXJve1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDBweDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDFlbSAwZW07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC5tZWRpYXNvc2lhbHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMWVtIDBlbTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNjBweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDEwcHggMHB4IDEwcHggMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgaW1ne1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgIC5nb29nbGV7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogNXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1MWUwZDQgO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogNHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAuZ29vZ2xlOmhvdmVye1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IzM5YTg5ZiA7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAuZmFjZWJvb2t7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxZW07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogNXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgLmZhY2Vib29rOmhvdmVye1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IzM5YTg5ZiA7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAucGVtaXNhaHtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAxZW0gMGVtO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICNidG5Ba3VuQntcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMWVtO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XG5cbiAgICB9XG59XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3Ljk4cHgpIHsgXG4ud3JhcHBlcntcbiAgICAucm93e1xuICAgICAgICAuYmFnaWFuMXtcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICAgIH1cbiAgICAgICAgLmJhZ2lhbjJ7XG4gICAgICAgICAgICAuYXRhc3tcbiAgICAgICAgICAgICAgICAuYmFnaWFuYXRhc3tcbiAgICAgICAgICAgICAgICAgICAgLmxvZ29ie1xuICAgICAgICAgICAgICAgICAgICAgICAgI2xvZ29Mb3V0YXJve1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgIH1cbn1cbiAgICBcblxufSIsImJvZHkge1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMHB4O1xufVxuYm9keSBwLCBib2R5IGgxLCBib2R5IGgyLCBib2R5IGgyLCBib2R5IGgzLCBib2R5IGg0LCBib2R5IGg1LCBib2R5IGg2IHtcbiAgbWFyZ2luOiAwcHg7XG59XG5cbi53cmFwcGVyIC5yb3cgLmJhZ2lhbjEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTFlMGQ0O1xuICBwYWRkaW5nOiAxMHB4O1xufVxuLndyYXBwZXIgLnJvdyAuYmFnaWFuMSAud3JhcHBlcjEge1xuICBwYWRkaW5nLXRvcDogMmVtO1xuICBtYXJnaW46IDBlbSAyZW0gMGVtIDJlbTtcbn1cbi53cmFwcGVyIC5yb3cgLmJhZ2lhbjEgLndyYXBwZXIxIC5sb2dvICNsb2dvTG91dGFybyB7XG4gIHdpZHRoOiAxNzJweDtcbn1cbi53cmFwcGVyIC5yb3cgLmJhZ2lhbjEgLndyYXBwZXIxIC50ZXh0U2xvZ2FuIHtcbiAgbWFyZ2luLXRvcDogMmVtO1xufVxuLndyYXBwZXIgLnJvdyAuYmFnaWFuMiB7XG4gIHBhZGRpbmc6IDIwcHg7XG59XG4ud3JhcHBlciAucm93IC5iYWdpYW4yIC5hdGFzIHtcbiAgbWFyZ2luLXRvcDogM2VtO1xufVxuLndyYXBwZXIgLnJvdyAuYmFnaWFuMiAuYXRhcyAuYmFnaWFuYXRhcyAubG9nb2IgI2xvZ29Mb3V0YXJvIHtcbiAgd2lkdGg6IDEwMHB4O1xuICBtYXJnaW46IDFlbSAwZW07XG4gIGRpc3BsYXk6IG5vbmU7XG59XG4ud3JhcHBlciAucm93IC5iYWdpYW4yIC5hdGFzIC5iYWdpYW5hdGFzIC5tZWRpYXNvc2lhbCB7XG4gIG1hcmdpbjogMWVtIDBlbTtcbiAgaGVpZ2h0OiA2MHB4O1xuICBwYWRkaW5nOiAxMHB4IDBweCAxMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbn1cbi53cmFwcGVyIC5yb3cgLmJhZ2lhbjIgLmF0YXMgLmJhZ2lhbmF0YXMgLm1lZGlhc29zaWFsIGltZyB7XG4gIHdpZHRoOiAzMHB4O1xufVxuLndyYXBwZXIgLnJvdyAuYmFnaWFuMiAuYXRhcyAuYmFnaWFuYXRhcyAubWVkaWFzb3NpYWwgLmdvb2dsZSB7XG4gIHBhZGRpbmc6IDVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzUxZTBkNDtcbiAgZGlzcGxheTogZmxleDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLndyYXBwZXIgLnJvdyAuYmFnaWFuMiAuYXRhcyAuYmFnaWFuYXRhcyAubWVkaWFzb3NpYWwgLmdvb2dsZSBwIHtcbiAgcGFkZGluZzogNHB4O1xuICBmb250LXNpemU6IDEycHg7XG59XG4ud3JhcHBlciAucm93IC5iYWdpYW4yIC5hdGFzIC5iYWdpYW5hdGFzIC5tZWRpYXNvc2lhbCAuZ29vZ2xlOmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM5YTg5Zjtcbn1cbi53cmFwcGVyIC5yb3cgLmJhZ2lhbjIgLmF0YXMgLmJhZ2lhbmF0YXMgLm1lZGlhc29zaWFsIC5mYWNlYm9vayB7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgbWFyZ2luLWxlZnQ6IDFlbTtcbiAgcGFkZGluZzogNXB4O1xufVxuLndyYXBwZXIgLnJvdyAuYmFnaWFuMiAuYXRhcyAuYmFnaWFuYXRhcyAubWVkaWFzb3NpYWwgLmZhY2Vib29rOmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM5YTg5Zjtcbn1cbi53cmFwcGVyIC5yb3cgLmJhZ2lhbjIgLmF0YXMgLnBlbWlzYWgge1xuICBtYXJnaW46IDFlbSAwZW07XG59XG4ud3JhcHBlciAucm93IC5iYWdpYW4yIC5hdGFzICNidG5Ba3VuQiB7XG4gIG1hcmdpbi10b3A6IDFlbTtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjcuOThweCkge1xuICAud3JhcHBlciAucm93IC5iYWdpYW4xIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG4gIC53cmFwcGVyIC5yb3cgLmJhZ2lhbjIgLmF0YXMgLmJhZ2lhbmF0YXMgLmxvZ29iICNsb2dvTG91dGFybyB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cbn0iXX0= */");

/***/ }),

/***/ "./src/app/register/register.component.ts":
/*!************************************************!*\
  !*** ./src/app/register/register.component.ts ***!
  \************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let RegisterComponent = class RegisterComponent {
    constructor() { }
    ngOnInit() {
    }
};
RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-register',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./register.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./register.component.scss */ "./src/app/register/register.component.scss")).default]
    })
], RegisterComponent);



/***/ }),

/***/ "./src/app/support/support.component.scss":
/*!************************************************!*\
  !*** ./src/app/support/support.component.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wrapper {\n  background-color: #F2F2F2;\n}\n.wrapper p {\n  font-size: 12px;\n  margin: 0px;\n}\n.wrapper h1, .wrapper h2, .wrapper h3, .wrapper h4, .wrapper h5, .wrapper h6 {\n  margin: 0px;\n}\n.wrapper .container .wrapper1 {\n  margin-top: 2em;\n  margin-bottom: 2em;\n  background-color: white;\n  border-radius: 3px;\n}\n.wrapper .container .wrapper1 .head {\n  margin-top: 1em;\n}\n.wrapper .container .wrapper1 .content {\n  padding: 0px 15px;\n}\n.wrapper .container .wrapper1 .content .space {\n  margin-top: 20px;\n}\n.wrapper .container .wrapper1 .content #subTitle {\n  margin-bottom: 10px;\n  color: #39dbcd;\n}\n.wrapper .container .wrapper1 .content .form-control {\n  margin-top: 10px;\n}\n.wrapper .container .wrapper1 .content .submit {\n  margin-bottom: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tZWRpYS9mYXJoYW5hcnlhL0RhdGEvcHJveWVrL3BrdGkxL0xvZ2luUjEvc3JjL2FwcC9zdXBwb3J0L3N1cHBvcnQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3N1cHBvcnQvc3VwcG9ydC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQVNJLHlCQUFBO0FDUEo7QURESTtFQUNJLGVBQUE7RUFDQSxXQUFBO0FDR1I7QURESTtFQUNJLFdBQUE7QUNHUjtBRElRO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQ0ZaO0FER1k7RUFDSSxlQUFBO0FDRGhCO0FER1k7RUFDSSxpQkFBQTtBQ0RoQjtBREVnQjtFQUNJLGdCQUFBO0FDQXBCO0FERWdCO0VBQ0ksbUJBQUE7RUFDQSxjQUFBO0FDQXBCO0FERWdCO0VBQ0ksZ0JBQUE7QUNBcEI7QURFZ0I7RUFDSSxtQkFBQTtBQ0FwQiIsImZpbGUiOiJzcmMvYXBwL3N1cHBvcnQvc3VwcG9ydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi53cmFwcGVye1xuICAgIHAge1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgIH1cbiAgICBoMSxoMixoMyxoNCxoNSxoNntcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgfVxuXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0YyRjJGMjtcblxuICAgIC5jb250YWluZXJ7XG4gICAgICAgIFxuICAgICAgICAud3JhcHBlcjF7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyZW07XG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAyZW07XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6M3B4IDtcbiAgICAgICAgICAgIC5oZWFke1xuICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDFlbTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5jb250ZW50e1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxNXB4O1xuICAgICAgICAgICAgICAgIC5zcGFjZXtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgI3N1YlRpdGxle1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzM5ZGJjZDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLmZvcm0tY29udHJvbHtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLnN1Ym1pdHtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgIH1cbn1cbiIsIi53cmFwcGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0YyRjJGMjtcbn1cbi53cmFwcGVyIHAge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbjogMHB4O1xufVxuLndyYXBwZXIgaDEsIC53cmFwcGVyIGgyLCAud3JhcHBlciBoMywgLndyYXBwZXIgaDQsIC53cmFwcGVyIGg1LCAud3JhcHBlciBoNiB7XG4gIG1hcmdpbjogMHB4O1xufVxuLndyYXBwZXIgLmNvbnRhaW5lciAud3JhcHBlcjEge1xuICBtYXJnaW4tdG9wOiAyZW07XG4gIG1hcmdpbi1ib3R0b206IDJlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn1cbi53cmFwcGVyIC5jb250YWluZXIgLndyYXBwZXIxIC5oZWFkIHtcbiAgbWFyZ2luLXRvcDogMWVtO1xufVxuLndyYXBwZXIgLmNvbnRhaW5lciAud3JhcHBlcjEgLmNvbnRlbnQge1xuICBwYWRkaW5nOiAwcHggMTVweDtcbn1cbi53cmFwcGVyIC5jb250YWluZXIgLndyYXBwZXIxIC5jb250ZW50IC5zcGFjZSB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG4ud3JhcHBlciAuY29udGFpbmVyIC53cmFwcGVyMSAuY29udGVudCAjc3ViVGl0bGUge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBjb2xvcjogIzM5ZGJjZDtcbn1cbi53cmFwcGVyIC5jb250YWluZXIgLndyYXBwZXIxIC5jb250ZW50IC5mb3JtLWNvbnRyb2wge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLndyYXBwZXIgLmNvbnRhaW5lciAud3JhcHBlcjEgLmNvbnRlbnQgLnN1Ym1pdCB7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/support/support.component.ts":
/*!**********************************************!*\
  !*** ./src/app/support/support.component.ts ***!
  \**********************************************/
/*! exports provided: SupportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportComponent", function() { return SupportComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SupportComponent = class SupportComponent {
    constructor() { }
    ngOnInit() {
    }
};
SupportComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-support',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./support.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/support/support.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./support.component.scss */ "./src/app/support/support.component.scss")).default]
    })
], SupportComponent);



/***/ }),

/***/ "./src/app/ulasan-toko/ulasan-toko.component.scss":
/*!********************************************************!*\
  !*** ./src/app/ulasan-toko/ulasan-toko.component.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wrapper {\n  margin-top: 2em;\n}\n.wrapper p {\n  font-size: 20px;\n  margin: 0px;\n}\n.wrapper h1, .wrapper h2, .wrapper h3, .wrapper h4, .wrapper h5, .wrapper h6 {\n  margin: 0px;\n}\n.wrapper .container-fluid {\n  width: 70%;\n}\n.wrapper .container-fluid .headToko .headProfil .picture {\n  padding: 10px;\n  width: 140px;\n}\n.wrapper .container-fluid .headToko .headProfil .picture .ava {\n  width: 150px;\n  border: 2px solid gray;\n}\n.wrapper .container-fluid .headToko .headProfil .desc .desc1 {\n  margin-top: 10px;\n}\n.wrapper .container-fluid .garis {\n  margin: 2em 0em;\n}\n.wrapper .container-fluid .navbar {\n  width: 100%;\n}\n.wrapper .container-fluid .navbar .ulasan a {\n  border-top: 1px solid gray;\n}\n.wrapper .container-fluid .navbar a {\n  cursor: pointer;\n  padding-top: 10px;\n  color: black;\n  text-decoration: none;\n}\n.wrapper .container-fluid .navbar a:hover {\n  border-top: 1px solid gray;\n}\n.wrapper .container-fluid .ulasanD #kategori {\n  margin-top: 2em;\n}\n.wrapper .container-fluid .ulasanD img {\n  width: 156px;\n  border-radius: 3px;\n}\n.wrapper .container-fluid .ulasanD .media {\n  margin-top: 2em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tZWRpYS9mYXJoYW5hcnlhL0RhdGEvcHJveWVrL3BrdGkxL0xvZ2luUjEvc3JjL2FwcC91bGFzYW4tdG9rby91bGFzYW4tdG9rby5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvdWxhc2FuLXRva28vdWxhc2FuLXRva28uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFRSSxlQUFBO0FDTko7QURESTtFQUNJLGVBQUE7RUFDQSxXQUFBO0FDR1I7QURESTtFQUNJLFdBQUE7QUNHUjtBRENJO0VBQ0ksVUFBQTtBQ0NSO0FERWdCO0VBQ0ksYUFBQTtFQUNBLFlBQUE7QUNBcEI7QURDb0I7RUFDSSxZQUFBO0VBQ0Esc0JBQUE7QUNDeEI7QURHb0I7RUFDSSxnQkFBQTtBQ0R4QjtBRE1RO0VBQ0ksZUFBQTtBQ0paO0FETVE7RUFDSSxXQUFBO0FDSlo7QURNZ0I7RUFDSSwwQkFBQTtBQ0pwQjtBRE9ZO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0FDTGhCO0FEUVk7RUFDSSwwQkFBQTtBQ05oQjtBRFlZO0VBQ0ksZUFBQTtBQ1ZoQjtBRFlZO0VBQ0ksWUFBQTtFQUNBLGtCQUFBO0FDVmhCO0FEWVk7RUFDSSxlQUFBO0FDVmhCIiwiZmlsZSI6InNyYy9hcHAvdWxhc2FuLXRva28vdWxhc2FuLXRva28uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud3JhcHBlcntcbiAgICBwIHtcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICB9XG4gICAgaDEsaDIsaDMsaDQsaDUsaDZ7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgIH1cbiAgICBtYXJnaW4tdG9wOiAyZW07XG5cbiAgICAuY29udGFpbmVyLWZsdWlke1xuICAgICAgICB3aWR0aDo3MCU7ICAgICAgICBcbiAgICAgICAgLmhlYWRUb2tve1xuICAgICAgICAgICAgLmhlYWRQcm9maWx7XG4gICAgICAgICAgICAgICAgLnBpY3R1cmV7XG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxNDBweDtcbiAgICAgICAgICAgICAgICAgICAgLmF2YXtcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxNTBweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlcjogMnB4IHNvbGlkIGdyYXk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLmRlc2N7XG4gICAgICAgICAgICAgICAgICAgIC5kZXNjMXtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmdhcmlze1xuICAgICAgICAgICAgbWFyZ2luOjJlbSAwZW0gO1xuICAgICAgICB9XG4gICAgICAgIC5uYXZiYXJ7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIC51bGFzYW57XG4gICAgICAgICAgICAgICAgYXtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGdyYXk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYXtcbiAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGE6aG92ZXJ7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGdyYXk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgLnVsYXNhbkR7XG4gICAgICAgICAgICAja2F0ZWdvcml7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMmVtO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaW1ne1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxNTZweDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAubWVkaWF7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMmVtO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbn0iLCIud3JhcHBlciB7XG4gIG1hcmdpbi10b3A6IDJlbTtcbn1cbi53cmFwcGVyIHAge1xuICBmb250LXNpemU6IDIwcHg7XG4gIG1hcmdpbjogMHB4O1xufVxuLndyYXBwZXIgaDEsIC53cmFwcGVyIGgyLCAud3JhcHBlciBoMywgLndyYXBwZXIgaDQsIC53cmFwcGVyIGg1LCAud3JhcHBlciBoNiB7XG4gIG1hcmdpbjogMHB4O1xufVxuLndyYXBwZXIgLmNvbnRhaW5lci1mbHVpZCB7XG4gIHdpZHRoOiA3MCU7XG59XG4ud3JhcHBlciAuY29udGFpbmVyLWZsdWlkIC5oZWFkVG9rbyAuaGVhZFByb2ZpbCAucGljdHVyZSB7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHdpZHRoOiAxNDBweDtcbn1cbi53cmFwcGVyIC5jb250YWluZXItZmx1aWQgLmhlYWRUb2tvIC5oZWFkUHJvZmlsIC5waWN0dXJlIC5hdmEge1xuICB3aWR0aDogMTUwcHg7XG4gIGJvcmRlcjogMnB4IHNvbGlkIGdyYXk7XG59XG4ud3JhcHBlciAuY29udGFpbmVyLWZsdWlkIC5oZWFkVG9rbyAuaGVhZFByb2ZpbCAuZGVzYyAuZGVzYzEge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLndyYXBwZXIgLmNvbnRhaW5lci1mbHVpZCAuZ2FyaXMge1xuICBtYXJnaW46IDJlbSAwZW07XG59XG4ud3JhcHBlciAuY29udGFpbmVyLWZsdWlkIC5uYXZiYXIge1xuICB3aWR0aDogMTAwJTtcbn1cbi53cmFwcGVyIC5jb250YWluZXItZmx1aWQgLm5hdmJhciAudWxhc2FuIGEge1xuICBib3JkZXItdG9wOiAxcHggc29saWQgZ3JheTtcbn1cbi53cmFwcGVyIC5jb250YWluZXItZmx1aWQgLm5hdmJhciBhIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgY29sb3I6IGJsYWNrO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG4ud3JhcHBlciAuY29udGFpbmVyLWZsdWlkIC5uYXZiYXIgYTpob3ZlciB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCBncmF5O1xufVxuLndyYXBwZXIgLmNvbnRhaW5lci1mbHVpZCAudWxhc2FuRCAja2F0ZWdvcmkge1xuICBtYXJnaW4tdG9wOiAyZW07XG59XG4ud3JhcHBlciAuY29udGFpbmVyLWZsdWlkIC51bGFzYW5EIGltZyB7XG4gIHdpZHRoOiAxNTZweDtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xufVxuLndyYXBwZXIgLmNvbnRhaW5lci1mbHVpZCAudWxhc2FuRCAubWVkaWEge1xuICBtYXJnaW4tdG9wOiAyZW07XG59Il19 */");

/***/ }),

/***/ "./src/app/ulasan-toko/ulasan-toko.component.ts":
/*!******************************************************!*\
  !*** ./src/app/ulasan-toko/ulasan-toko.component.ts ***!
  \******************************************************/
/*! exports provided: UlasanTokoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UlasanTokoComponent", function() { return UlasanTokoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let UlasanTokoComponent = class UlasanTokoComponent {
    constructor() { }
    ngOnInit() {
    }
};
UlasanTokoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ulasan-toko',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./ulasan-toko.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/ulasan-toko/ulasan-toko.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./ulasan-toko.component.scss */ "./src/app/ulasan-toko/ulasan-toko.component.scss")).default]
    })
], UlasanTokoComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!**********************************************************************************************************!*\
  !*** multi (webpack)-dev-server/client?http://0.0.0.0:0/sockjs-node&sockPath=/sockjs-node ./src/main.ts ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /media/farhanarya/Data/proyek/pkti1/LoginR1/node_modules/webpack-dev-server/client/index.js?http://0.0.0.0:0/sockjs-node&sockPath=/sockjs-node */"./node_modules/webpack-dev-server/client/index.js?http://0.0.0.0:0/sockjs-node&sockPath=/sockjs-node");
module.exports = __webpack_require__(/*! /media/farhanarya/Data/proyek/pkti1/LoginR1/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map