import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyTokoComponent } from './my-toko.component';

describe('MyTokoComponent', () => {
  let component: MyTokoComponent;
  let fixture: ComponentFixture<MyTokoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyTokoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyTokoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
