import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pengunjung',
  templateUrl: './pengunjung.component.html',
  styleUrls: ['./pengunjung.component.scss']
})
export class PengunjungComponent implements OnInit {


  public barChartOptions = {
  scaleShowverticalLines:false,
  responsive:true,
}

public barChartLabels = ['Januari', 'Februari', 'Maret', 'April', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
public barChartType = 'line';
public barChartLegend = false;

public barChartData = [
  {data: [10,12,11,6,7,7,12,5,9,10,11]
  }
]

  constructor() { }

  ngOnInit() {
  }

  

}
