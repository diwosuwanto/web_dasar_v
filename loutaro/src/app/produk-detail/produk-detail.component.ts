import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProdukService } from '../services/produk.service';

@Component({
  selector: 'app-produk-detail',
  templateUrl: './produk-detail.component.html',
  styleUrls: ['./produk-detail.component.scss']
})
export class ProdukDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private produkService: ProdukService) { }

  id: any;

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get("id");
  }

}
