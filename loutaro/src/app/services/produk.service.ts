import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProdukService {

  produk: any = {
    1: {
      title: 'Personalised Portrait Creator v7',
      img: 'assets/pic/produk1.jpg',
      regulerPrice: 150000,
      extendedPrice: 950000,
      author: 'Rachel Price'
    },
    2: {
      title: '2000 Infographics Templates Bundle',
      img: 'assets/pic/produk2.jpg',
      regulerPrice: 340000,
      extendedPrice: 1950000,
      author: 'Rachel Price'
    },
    3: {
      title: 'CMP Mercury - Coming soon',
      img: 'assets/pic/produk3.jpg',
      regulerPrice: 200000,
      extendedPrice: 850000,
      author: 'Rachel Price'
    },
    4: {
      title: 'Stockholm Premium Icons Pack',
      img: 'assets/pic/produk4.jpg',
      regulerPrice: 280000,
      extendedPrice: 980000,
      author: 'Rachel Price'
    },
    5: {
      title: '18 Hands & Logo Templates',
      img: 'assets/pic/produk5.jpg',
      regulerPrice: 260000,
      extendedPrice: 960000,
      author: 'Rachel Price'
    },
    6: {
      title: '700+ Premium Vector Icons',
      img: 'assets/pic/produk6.jpg',
      regulerPrice: 370000,
      extendedPrice: 2760000,
      author: 'Rachel Price'
    },
    7: {
      title: 'Portfolio Business Genesis theme An',
      img: 'assets/pic/produk7.jpg',
      regulerPrice: 620000,
      extendedPrice: 3760500,
      author: 'Rachel Price'
    },
    8: {
      title: 'Real Estate WordPress WPCasa',
      img: 'assets/pic/produk8.jpg',
      regulerPrice: 700000,
      extendedPrice: 5165500,
      author: 'Rachel Price'
    },
    9: {
      title: 'Big Kids Collection Illustration',
      img: 'assets/pic/produk9.jpg',
      regulerPrice: 350000,
      extendedPrice: 1500000,
      author: 'Rachel Price'
    },
    10: {
      title: 'VR Icons and Typography',
      img: 'assets/pic/produk10.jpg',
      regulerPrice: 340000,
      extendedPrice: 1400000,
      author: 'Rachel Price'
    },
    11: {
      title: 'Emily Grace - A Blog & Shop Theme',
      img: 'assets/pic/produk11.jpg',
      regulerPrice: 670000,
      extendedPrice: 3400000,
      author: 'Rachel Price'
    },
    12: {
      title: 'Eram - Photography & Portfolio Theme',
      img: 'assets/pic/produk12.jpg',
      regulerPrice: 720000,
      extendedPrice: 5500000,
      author: 'Rachel Price'
    }
  }

  popular: any = {
    1: {
      title: 'Personalised Portrait Creator v7',
      img: 'assets/pic/produk1.jpg',
      regulerPrice: 150000,
      extendedPrice: 950000,
      author: 'Rachel Price'
    },
    2: {
      title: '2000 Infographics Templates Bundle',
      img: 'assets/pic/produk2.jpg',
      regulerPrice: 340000,
      extendedPrice: 1950000,
      author: 'Rachel Price'
    },
    3: {
      title: 'CMP Mercury - Coming soon',
      img: 'assets/pic/produk3.jpg',
      regulerPrice: 200000,
      extendedPrice: 850000,
      author: 'Rachel Price'
    },
    4: {
      title: 'Stockholm Premium Icons Pack',
      img: 'assets/pic/produk4.jpg',
      regulerPrice: 280000,
      extendedPrice: 980000,
      author: 'Rachel Price'
    }
  }

  featured: any = {
    5: {
      title: '18 Hands & Logo Templates',
      img: 'assets/pic/produk5.jpg',
      regulerPrice: 260000,
      extendedPrice: 960000,
      author: 'Rachel Price'
    },
    6: {
      title: '700+ Premium Vector Icons',
      img: 'assets/pic/produk6.jpg',
      regulerPrice: 370000,
      extendedPrice: 2760000,
      author: 'Rachel Price'
    },
    7: {
      title: 'Portfolio Business Genesis theme An',
      img: 'assets/pic/produk7.jpg',
      regulerPrice: 620000,
      extendedPrice: 3760500,
      author: 'Rachel Price'
    },
    8: {
      title: 'Real Estate WordPress WPCasa',
      img: 'assets/pic/produk8.jpg',
      regulerPrice: 700000,
      extendedPrice: 5165500,
      author: 'Rachel Price'
    }
  }

  trending: any = {
    9: {
      title: 'Big Kids Collection Illustration',
      img: 'assets/pic/produk9.jpg',
      regulerPrice: 350000,
      extendedPrice: 1500000,
      author: 'Rachel Price'
    },
    10: {
      title: 'VR Icons and Typography',
      img: 'assets/pic/produk10.jpg',
      regulerPrice: 340000,
      extendedPrice: 1400000,
      author: 'Rachel Price'
    },
    11: {
      title: 'Emily Grace - A Blog & Shop Theme',
      img: 'assets/pic/produk11.jpg',
      regulerPrice: 670000,
      extendedPrice: 3400000,
      author: 'Rachel Price'
    },
    12: {
      title: 'Eram - Photography & Portfolio Theme',
      img: 'assets/pic/produk12.jpg',
      regulerPrice: 720000,
      extendedPrice: 5500000,
      author: 'Rachel Price'
    }
  }

  constructor() { }
}
