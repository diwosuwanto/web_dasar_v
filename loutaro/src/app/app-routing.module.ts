import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../app/login/login.component';
import { RegisterComponent } from '../app/register/register.component';
import { MetodePembayaranComponent } from '../app/metode-pembayaran/metode-pembayaran.component';
import { SupportComponent } from '../app/support/support.component';
import { ProfileTokoComponent } from '../app/profile-toko/profile-toko.component';
import { ProdukPTokoComponent } from '../app/produk-ptoko/produk-ptoko.component';
import { UlasanTokoComponent } from '../app/ulasan-toko/ulasan-toko.component';
import { AboutTokoComponent } from '../app/about-toko/about-toko.component';
import { ChatComponent } from '../app/chat/chat.component';
import { MyTokoComponent } from '../app/my-toko/my-toko.component';
import { ProdukTokoComponent } from '../app/produk-toko/produk-toko.component';
import { PengunjungComponent } from '../app/pengunjung/pengunjung.component';
import { NavbarTokoComponent } from '../app/navbar-toko/navbar-toko.component';
import { WishlistComponent } from '../app/wishlist/wishlist.component';
import { TokoFavoriteComponent } from '../app/toko-favorite/toko-favorite.component';
import { HomeComponent } from './home/home.component';
import { CartComponent } from './cart/cart.component';
import { ProdukDetailComponent } from './produk-detail/produk-detail.component';
import { ProdukListComponent } from './produk-list/produk-list.component';
import { UploadComponent } from './upload/upload.component';
import { MyItemComponent } from './my-item/my-item.component';


const routes: Routes = [
  {path: '', component : HomeComponent},
  {path: 'cart', component : CartComponent},
  {path: 'upload', component : UploadComponent},
  {path: 'produkDetail/:id', component : ProdukDetailComponent},
  {path: 'produkList/:category', component : ProdukListComponent},
  {path: 'login', component : LoginComponent},
  {path: 'register', component : RegisterComponent},
  {path: 'metodePembayaran', component : MetodePembayaranComponent},
  {path: 'support', component : SupportComponent},
  {path: 'profileToko', component: ProfileTokoComponent},
  {path: 'profileProduk', component: ProdukPTokoComponent},
  {path: 'profileUlasan', component: UlasanTokoComponent},
  {path: 'profileAbout', component: AboutTokoComponent},
  {path: 'Chat', component: ChatComponent},
  {path: 'myToko',component:MyTokoComponent},
  {path: 'myTokoProduk', component:ProdukTokoComponent},
  {path: 'myPengunjung', component:PengunjungComponent},
  {path: 'navbarToko', component:NavbarTokoComponent},
  {path: 'wishlistToko', component:WishlistComponent},
  {path: 'myItem', component:MyItemComponent},
  {path: 'favoriteToko', component:TokoFavoriteComponent }


  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
