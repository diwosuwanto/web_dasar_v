import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutTokoComponent } from './about-toko.component';

describe('AboutTokoComponent', () => {
  let component: AboutTokoComponent;
  let fixture: ComponentFixture<AboutTokoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutTokoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutTokoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
