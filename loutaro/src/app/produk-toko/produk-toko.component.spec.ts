import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdukTokoComponent } from './produk-toko.component';

describe('ProdukTokoComponent', () => {
  let component: ProdukTokoComponent;
  let fixture: ComponentFixture<ProdukTokoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdukTokoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdukTokoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
