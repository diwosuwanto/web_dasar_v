import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileTokoComponent } from './profile-toko.component';

describe('ProfileTokoComponent', () => {
  let component: ProfileTokoComponent;
  let fixture: ComponentFixture<ProfileTokoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileTokoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileTokoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
