import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { MetodePembayaranComponent } from './metode-pembayaran/metode-pembayaran.component';
import { SupportComponent } from './support/support.component';
import { ProfileTokoComponent } from './profile-toko/profile-toko.component';
import { ProdukPTokoComponent } from './produk-ptoko/produk-ptoko.component';
import { UlasanTokoComponent } from './ulasan-toko/ulasan-toko.component';
import { AboutTokoComponent } from './about-toko/about-toko.component';
import { ChatComponent } from './chat/chat.component';
import { MyTokoComponent } from './my-toko/my-toko.component';
import { NavbarTokoComponent } from './navbar-toko/navbar-toko.component';
import { ProdukTokoComponent } from './produk-toko/produk-toko.component';
import { PengunjungComponent } from './pengunjung/pengunjung.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { TokoFavoriteComponent } from './toko-favorite/toko-favorite.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CartComponent } from './cart/cart.component';
import { ProdukDetailComponent } from './produk-detail/produk-detail.component';
import { ProdukListComponent } from './produk-list/produk-list.component';
import { UploadComponent } from './upload/upload.component';
import { MyItemComponent } from './my-item/my-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    RegisterComponent,
    LoginComponent,
    MetodePembayaranComponent,
    SupportComponent,
    ProfileTokoComponent,
    ProdukPTokoComponent,
    UlasanTokoComponent,
    AboutTokoComponent,
    ChatComponent,
    MyTokoComponent,
    NavbarTokoComponent,
    ProdukTokoComponent,
    PengunjungComponent,
    WishlistComponent,
    TokoFavoriteComponent,
    CartComponent,
    ProdukDetailComponent,
    ProdukListComponent,
    UploadComponent,
    MyItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
