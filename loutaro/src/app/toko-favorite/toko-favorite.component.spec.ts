import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TokoFavoriteComponent } from './toko-favorite.component';

describe('TokoFavoriteComponent', () => {
  let component: TokoFavoriteComponent;
  let fixture: ComponentFixture<TokoFavoriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TokoFavoriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TokoFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
