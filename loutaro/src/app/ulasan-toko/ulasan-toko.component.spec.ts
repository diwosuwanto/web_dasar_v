import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UlasanTokoComponent } from './ulasan-toko.component';

describe('UlasanTokoComponent', () => {
  let component: UlasanTokoComponent;
  let fixture: ComponentFixture<UlasanTokoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UlasanTokoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UlasanTokoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
