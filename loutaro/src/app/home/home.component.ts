import { Component, OnInit } from '@angular/core';
import { ProdukService } from '../services/produk.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private produkService: ProdukService) { }

  ngOnInit() {
    console.log(this.produkService.popular);
  }

}
