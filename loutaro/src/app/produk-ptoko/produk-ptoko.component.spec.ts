import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdukPTokoComponent } from './produk-ptoko.component';

describe('ProdukPTokoComponent', () => {
  let component: ProdukPTokoComponent;
  let fixture: ComponentFixture<ProdukPTokoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdukPTokoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdukPTokoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
