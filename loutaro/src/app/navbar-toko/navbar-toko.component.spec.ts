import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarTokoComponent } from './navbar-toko.component';

describe('NavbarTokoComponent', () => {
  let component: NavbarTokoComponent;
  let fixture: ComponentFixture<NavbarTokoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarTokoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarTokoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
