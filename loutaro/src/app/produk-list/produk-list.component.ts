import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProdukService } from '../services/produk.service';

@Component({
  selector: 'app-produk-list',
  templateUrl: './produk-list.component.html',
  styleUrls: ['./produk-list.component.scss']
})
export class ProdukListComponent implements OnInit {

  constructor(private route: ActivatedRoute, private produkService: ProdukService) { }

  category:any;

  ngOnInit() {
    this.category = this.route.snapshot.paramMap.get("category");
  }

}
